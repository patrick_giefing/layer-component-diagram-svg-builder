"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DiagramElementDirection = exports.LayerComponentDiagramNodeSorter = exports.LayerComponentDiagramReader = void 0;
const layer_component_diagram_1 = require("./layer.component.diagram");
const diagram_node_position_1 = require("./diagram.node.position");
const layer_component_diagram_area_1 = require("./layer.component.diagram.area");
const layer_component_diagram_node_1 = require("./layer.component.diagram.node");
class LayerComponentDiagramReader {
    constructor(drawingArea) {
        this.drawingArea = drawingArea;
    }
    static readFromRepresentation(drawingArea) {
        let diagramReader = new LayerComponentDiagramReader(drawingArea);
        let markerProperties = layer_component_diagram_area_1.LayerComponentDiagramMarkerPropertiesReader.readString(drawingArea.getDiagramString());
        let topLeftCorners = drawingArea.findTopLeftCorners();
        let nodes = topLeftCorners.map(topLeftCorner => diagramReader.readNodeFromTopLeftCorner(topLeftCorner));
        nodes = nodes.filter(it => it != null);
        nodes.forEach(node => {
            let markers = node.getMarkers();
            if (!markers)
                return;
            markers.forEach(marker => marker.properties = markerProperties[marker.getName()]);
        });
        nodes = LayerComponentDiagramNodeSorter.sortNodes(nodes);
        return new layer_component_diagram_1.LayerComponentDiagram(nodes);
    }
    readNodeFromTopLeftCorner(startPosition) {
        let borderTraverser = new layer_component_diagram_area_1.LayerComponentDiagramAreaBorderTraverser(this.drawingArea);
        let border = borderTraverser.traverseAndGetBorder(startPosition);
        if (border == null)
            return null;
        let diagramNode = this.createDiagramNodeFromBorder(border);
        return diagramNode;
    }
    createDiagramNodeFromBorder(border) {
        let width = this.drawingArea.getWidth();
        let height = this.drawingArea.getHeight();
        let borderSymbols = new Array(height).fill(" ").map(() => new Array(width).fill(" "));
        let markers = [];
        for (let y = 0; y < height; y++) {
            let borderOpen = false;
            for (let x = 0; x < width; x++) {
                let position = diagram_node_position_1.DiagramNodePosition.of(x, y);
                let symbol = this.drawingArea.getSymbolAt(position);
                let isBorderAtPosition = border[y][x];
                if (isBorderAtPosition) {
                    borderOpen = !borderOpen;
                    borderSymbols[y][x] = symbol;
                }
                else if (borderOpen) {
                    let checkForContentMarker = layer_component_diagram_area_1.LayerComponentDiagramArea.isContentStartMarkerSymbol(symbol);
                    if (checkForContentMarker) {
                        let markerContentStart = x + 1;
                        let xMarkerEnd = this.getContentEndMarkerSymbolPositionFromOffset(markerContentStart, y);
                        let xBorderEnd = this.getClosingBorderPositionFromOffset(border[y], x);
                        if (xMarkerEnd == -1 || xBorderEnd < xMarkerEnd) {
                            xMarkerEnd = xBorderEnd;
                        }
                        let contentMarkerName = "";
                        for (let xMarker = markerContentStart; xMarker < xMarkerEnd; xMarker++) {
                            contentMarkerName += this.drawingArea.getSymbolAt(diagram_node_position_1.DiagramNodePosition.of(xMarker, y));
                        }
                        if (contentMarkerName.length > 0) {
                            markers.push(layer_component_diagram_area_1.LayerComponentDiagramMarker.ofOffsetAndName(diagram_node_position_1.DiagramNodePosition.of(x, y), contentMarkerName));
                        }
                    }
                }
            }
        }
        return layer_component_diagram_node_1.LayerComponentDiagramNode.ofPattern(borderSymbols, markers);
    }
    getContentEndMarkerSymbolPositionFromOffset(offsetX, offsetY) {
        for (let x = offsetX; x < this.drawingArea.getWidth(); x++) {
            let symbol = this.drawingArea.getSymbolAt(diagram_node_position_1.DiagramNodePosition.of(x, offsetY));
            if (!symbol.match(/[a-zA-Z0-9_-]/i))
                return x;
        }
        return -1;
    }
    getClosingBorderPositionFromOffset(borderLine, offset) {
        for (let x = offset; x < borderLine.length; x++) {
            if (borderLine[x])
                return x;
        }
        return -1;
    }
}
exports.LayerComponentDiagramReader = LayerComponentDiagramReader;
class LayerComponentDiagramNodeSorter {
    static sortNodes(nodes) {
        let nodesSorted = nodes.sort((a, b) => {
            if (a == null)
                return -1;
            if (b == null)
                return 1;
            let ax = a.getOffset().getX();
            let bx = b.getOffset().getX();
            if (ax != bx) {
                return ax < bx ? -1 : 1;
            }
            let ay = a.getOffset().getY();
            let by = b.getOffset().getY();
            return ay < by ? -1 : 1;
        });
        return nodesSorted;
    }
}
exports.LayerComponentDiagramNodeSorter = LayerComponentDiagramNodeSorter;
var DiagramElementDirection;
(function (DiagramElementDirection) {
    DiagramElementDirection[DiagramElementDirection["RIGHT"] = 0] = "RIGHT";
    DiagramElementDirection[DiagramElementDirection["DOWN"] = 1] = "DOWN";
    DiagramElementDirection[DiagramElementDirection["LEFT"] = 2] = "LEFT";
    DiagramElementDirection[DiagramElementDirection["UP"] = 3] = "UP";
})(DiagramElementDirection = exports.DiagramElementDirection || (exports.DiagramElementDirection = {}));
//# sourceMappingURL=layer.component.diagram.reader.js.map