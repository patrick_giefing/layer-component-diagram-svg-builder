"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.LayerComponentDiagramSvgStringBuilder = void 0;
const layer_component_diagram_composite_node_area_1 = require("./layer.component.diagram.composite.node.area");
const layer_component_diagram_reader_1 = require("./layer.component.diagram.reader");
const layer_component_diagram_node_svg_builder_1 = require("./layer.component.diagram.node.svg.builder");
class LayerComponentDiagramSvgStringBuilder {
    createSvgElementStringFromString(s) {
        let representation = new layer_component_diagram_composite_node_area_1.LayerComponentDiagramCompositeNodeArea(s);
        let svgContentString = this.createSvgContentElementsString(representation);
        let width = (representation.getWidth() + 100) * 100;
        let height = (representation.getHeight() + 100) * 100;
        let svgElementString = `<svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 ${width} ${height}">${svgContentString}</svg>`;
        return svgElementString;
    }
    createSvgContentElementsStringFromString(s) {
        let representation = new layer_component_diagram_composite_node_area_1.LayerComponentDiagramCompositeNodeArea(s);
        let svgContentString = this.createSvgContentElementsString(representation);
        return svgContentString;
    }
    createSvgContentElementsString(representation) {
        let diagram = layer_component_diagram_reader_1.LayerComponentDiagramReader.readFromRepresentation(representation);
        let nodes = diagram.getNodes();
        let nodeSvgPathBuilder = new layer_component_diagram_node_svg_builder_1.LayerComponentDiagramNodeSvgPathBuilder();
        let svgContentString = "";
        nodes.forEach(node => {
            let svgPathElementString = nodeSvgPathBuilder.createSvgPathElementString(node);
            svgContentString += svgPathElementString + "\r\n";
        });
        nodes.forEach(node => {
            let svgTextElementString = nodeSvgPathBuilder.createSvgTextElementString(node);
            if (svgTextElementString)
                svgContentString += svgTextElementString + "\r\n";
        });
        return svgContentString;
    }
}
exports.LayerComponentDiagramSvgStringBuilder = LayerComponentDiagramSvgStringBuilder;
//# sourceMappingURL=layer.component.diagram.svg.builder.js.map