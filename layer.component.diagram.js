"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.LayerComponentDiagram = void 0;
class LayerComponentDiagram {
    constructor(nodes) {
        this.nodes = nodes;
    }
    getNodes() {
        return this.nodes;
    }
}
exports.LayerComponentDiagram = LayerComponentDiagram;
//# sourceMappingURL=layer.component.diagram.js.map