export class DiagramNodePosition {
    private constructor(private x: number, private y: number) {
    }

    public static of(x: number, y: number): DiagramNodePosition {
        return new DiagramNodePosition(x, y);
    }

    public getX(): number {
        return this.x;
    }

    public getY(): number {
        return this.y;
    }

    public addOffset(p: DiagramNodePosition): DiagramNodePosition {
        return DiagramNodePosition.of(this.getX() + p.getX(), this.getY() + p.getY());
    }

    public substractOffset(p: DiagramNodePosition): DiagramNodePosition {
        return DiagramNodePosition.of(this.getX() - p.getX(), this.getY() - p.getY());
    }
}