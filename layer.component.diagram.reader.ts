import {
    LayerComponentDiagram
} from "./layer.component.diagram";
import {DiagramNodePosition} from "./diagram.node.position";
import {
    LayerComponentDiagramArea,
    LayerComponentDiagramAreaBorderTraverser,
    LayerComponentDiagramMarker, LayerComponentDiagramMarkerPropertiesReader
} from "./layer.component.diagram.area";
import {LayerComponentDiagramNode} from "./layer.component.diagram.node";

export class LayerComponentDiagramReader {
    private constructor(private drawingArea: LayerComponentDiagramArea) {
    }

    public static readFromRepresentation(drawingArea: LayerComponentDiagramArea): LayerComponentDiagram {
        const diagramReader = new LayerComponentDiagramReader(drawingArea);
        const markerProperties = LayerComponentDiagramMarkerPropertiesReader.readString(drawingArea.getDiagramString())
        const topLeftCorners = drawingArea.findTopLeftCorners();
        let nodes = topLeftCorners.map(topLeftCorner => diagramReader.readNodeFromTopLeftCorner(topLeftCorner));
        nodes = nodes.filter(it => it != null);
        nodes.forEach(node => {
            const markers = node.getMarkers();
            if (!markers) return;
            markers.forEach(marker => marker.properties = markerProperties[marker.getName()]);
        });
        nodes = LayerComponentDiagramNodeSorter.sortNodes(nodes);
        return new LayerComponentDiagram(nodes);
    }

    private readNodeFromTopLeftCorner(startPosition: DiagramNodePosition): LayerComponentDiagramNode {
        const borderTraverser = new LayerComponentDiagramAreaBorderTraverser(this.drawingArea);
        const border = borderTraverser.traverseAndGetBorder(startPosition);
        if (border == null) return null;
        const diagramNode = this.createDiagramNodeFromBorder(border);
        return diagramNode;
    }

    private createDiagramNodeFromBorder(border: boolean[][]): LayerComponentDiagramNode {
        const width = this.drawingArea.getWidth();
        const height = this.drawingArea.getHeight();
        const borderSymbols: string[][] = new Array(height).fill(" ").map(() => new Array(width).fill(" "));
        const markers: LayerComponentDiagramMarker[] = [];
        for (let y = 0; y < height; y++) {
            let borderOpen = false;
            for (let x = 0; x < width; x++) {
                const position = DiagramNodePosition.of(x, y);
                const symbol = this.drawingArea.getSymbolAt(position);
                const isBorderAtPosition = border[y][x];
                if (isBorderAtPosition) {
                    borderOpen = !borderOpen;
                    borderSymbols[y][x] = symbol;
                } else if (borderOpen) {
                    const checkForContentMarker = LayerComponentDiagramArea.isContentStartMarkerSymbol(symbol);
                    if (checkForContentMarker) {
                        const markerContentStart = x + 1;
                        let xMarkerEnd = this.getContentEndMarkerSymbolPositionFromOffset(markerContentStart, y);
                        const xBorderEnd = this.getClosingBorderPositionFromOffset(border[y], x);
                        if (xMarkerEnd == -1 || xBorderEnd < xMarkerEnd) {
                            xMarkerEnd = xBorderEnd;
                        }
                        let contentMarkerName = "";
                        for (let xMarker = markerContentStart; xMarker < xMarkerEnd; xMarker++) {
                            contentMarkerName += this.drawingArea.getSymbolAt(DiagramNodePosition.of(xMarker, y));
                        }
                        if (contentMarkerName.length > 0) {
                            markers.push(LayerComponentDiagramMarker.ofOffsetAndName(DiagramNodePosition.of(x, y), contentMarkerName));
                        }
                    }
                }
            }
        }
        return LayerComponentDiagramNode.ofPattern(borderSymbols, markers);
    }

    private getContentEndMarkerSymbolPositionFromOffset(offsetX: number, offsetY: number): number {
        for (let x = offsetX; x < this.drawingArea.getWidth(); x++) {
            const symbol = this.drawingArea.getSymbolAt(DiagramNodePosition.of(x, offsetY));
            if (!symbol.match(/[a-zA-Z0-9_-]/i)) return x;
        }
        return -1;
    }

    private getClosingBorderPositionFromOffset(borderLine: boolean[], offset: number): number {
        for (let x = offset; x < borderLine.length; x++) {
            if (borderLine[x]) return x;
        }
        return -1;
    }
}

export class LayerComponentDiagramNodeSorter {
    public static sortNodes(nodes: LayerComponentDiagramNode[]): LayerComponentDiagramNode[] {
        const nodesSorted = nodes.sort((a: LayerComponentDiagramNode, b: LayerComponentDiagramNode): number => {
            if (a == null) return -1;
            if (b == null) return 1;
            const ax = a.getOffset().getX();
            const bx = b.getOffset().getX();
            if (ax != bx) {
                return ax < bx ? -1 : 1;
            }
            const ay = a.getOffset().getY();
            const by = b.getOffset().getY();
            return ay < by ? -1 : 1;
        });
        return nodesSorted;
    }
}

export enum DiagramElementDirection {
    RIGHT, DOWN, LEFT, UP
}