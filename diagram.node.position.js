"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DiagramNodePosition = void 0;
class DiagramNodePosition {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }
    static of(x, y) {
        return new DiagramNodePosition(x, y);
    }
    getX() {
        return this.x;
    }
    getY() {
        return this.y;
    }
    addOffset(p) {
        return DiagramNodePosition.of(this.getX() + p.getX(), this.getY() + p.getY());
    }
    substractOffset(p) {
        return DiagramNodePosition.of(this.getX() - p.getX(), this.getY() - p.getY());
    }
}
exports.DiagramNodePosition = DiagramNodePosition;
//# sourceMappingURL=diagram.node.position.js.map