import {DiagramNodePosition} from "./diagram.node.position";
import {LayerComponentDiagramArea, LayerComponentDiagramMarker} from "./layer.component.diagram.area";

export class LayerComponentDiagramNode extends LayerComponentDiagramArea {
    private constructor(private offset: DiagramNodePosition, private borderSymbols: string[][], private markers: LayerComponentDiagramMarker[] = []) {
        super(borderSymbols.map(borderSymbolsLine => borderSymbolsLine.join("")).join("\r\n"));
    }

    public getOffset(): DiagramNodePosition {
        return this.offset;
    }

    public getMarkers(): LayerComponentDiagramMarker[] {
        return this.markers;
    }

    public static ofPattern(board: string[][], markers: LayerComponentDiagramMarker[] = []): LayerComponentDiagramNode {
        const trimmer = new LayerComponentDiagramAreaTrimmer();
        const minBorderX = trimmer.getMinBorderX(board);
        const minBorderY = trimmer.getMinBorderY(board);
        board = trimmer.trim(board);

        const offset = DiagramNodePosition.of(minBorderX, minBorderY);
        markers = markers.map(marker => LayerComponentDiagramMarker.ofOffsetAndName(marker.getOffset().substractOffset(offset), marker.getName()));
        return new LayerComponentDiagramNode(offset, board, markers);
    }

    public static ofOffsetAndPattern(offset: DiagramNodePosition, borderSymbols: string[][], markers: LayerComponentDiagramMarker[] = []): LayerComponentDiagramNode {
        return new LayerComponentDiagramNode(offset, borderSymbols, markers);
    }
}

export class LayerComponentDiagramAreaTrimmer {
    public trim(board: string[][]): string[][] {
        board = this.trimTop(board);
        board = this.trimBottom(board);
        board = this.trimLeft(board);
        board = this.trimRight(board);
        return board;
    }

    public trimTop(board: string[][]): string[][] {
        const minBorderY = this.getMinBorderY(board);
        if (minBorderY > 0) {
            for (let i = 0; i < minBorderY; i++) {
                board.shift();
            }
        }
        return board;
    }

    public trimBottom(board: string[][]): string[][] {
        const maxBorderY = this.getMaxBorderY(board);
        if (maxBorderY > 0) {
            while (board.length > maxBorderY + 1) {
                board.pop();
            }
        }
        return board;
    }

    public trimLeft(board: string[][]): string[][] {
        const minBorderX = this.getMinBorderX(board);
        if (minBorderX > 0) {
            for (let y = 0; y < board.length; y++) {
                const borderSymbolsLine = board[y];
                for (let i = 0; i < minBorderX; i++) {
                    borderSymbolsLine.shift();
                }
            }
        }
        return board;
    }

    public trimRight(board: string[][]): string[][] {
        const maxBorderX = this.getMaxBorderX(board);
        if (maxBorderX > 0) {
            for (let y = 0; y < board.length; y++) {
                const borderSymbolsLine = board[y];
                while (borderSymbolsLine.length > maxBorderX + 1) {
                    borderSymbolsLine.pop();
                }
            }
        }
        return board;
    }

    public getMinBorderX(board: string[][]): number {
        let minIndex = -1;
        for (let y = 0; y < board.length; y++) {
            const borderSymbolsLine = board[y];
            for (let x = 0; x < borderSymbolsLine.length; x++) {
                const symbol = borderSymbolsLine[x];
                if (symbol != ' ') {
                    minIndex = minIndex == -1 ? x : Math.min(minIndex, x);
                    break;
                }
            }
        }
        return minIndex;
    }

    public getMaxBorderX(board: string[][]): number {
        let maxIndex = 0;
        for (let y = 0; y < board.length; y++) {
            const borderSymbolsLine = board[y];
            for (let x = borderSymbolsLine.length - 1; x >= 0; x--) {
                const symbol = borderSymbolsLine[x];
                if (symbol != ' ') {
                    maxIndex = Math.max(maxIndex, x);
                    break;
                }
            }
        }
        return maxIndex;
    }

    public getMinBorderY(board: string[][]): number {
        let minIndex = -1;
        for (let y = 0; y < board.length; y++) {
            const borderSymbolsLine = board[y];
            for (let x = 0; x < borderSymbolsLine.length; x++) {
                const symbol = borderSymbolsLine[x];
                if (symbol != ' ') {
                    minIndex = minIndex == -1 ? y : Math.min(minIndex, y);
                    break;
                }
            }
        }
        return minIndex;
    }

    public getMaxBorderY(board: string[][]): number {
        let minIndex = 0;
        for (let y = board.length - 1; y >= 0; y--) {
            const borderSymbolsLine = board[y];
            for (let x = 0; x < borderSymbolsLine.length; x++) {
                const symbol = borderSymbolsLine[x];
                if (symbol != ' ') {
                    minIndex = Math.max(minIndex, y);
                    break;
                }
            }
        }
        return minIndex;
    }
}