"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.LayerComponentDiagramNodeSvgPathBuilder = void 0;
const layer_component_diagram_area_1 = require("./layer.component.diagram.area");
class LayerComponentDiagramNodeSvgPathBuilder {
    constructor() {
        this.borderSvg = {};
        this.borderSvg[layer_component_diagram_area_1.LayerComponentDiagramBorderElement.LEFT_TO_TOP] = "l0,-50 l50,0";
        this.borderSvg[layer_component_diagram_area_1.LayerComponentDiagramBorderElement.LEFT_TO_TOP_ROUND] = "l0,-25 a25,-25 0 0,1 25,-25 l25,0";
        this.borderSvg[layer_component_diagram_area_1.LayerComponentDiagramBorderElement.TOP_TO_LEFT] = "l-50,0 l0,50";
        this.borderSvg[layer_component_diagram_area_1.LayerComponentDiagramBorderElement.TOP_TO_LEFT_ROUND] = this.borderSvg[layer_component_diagram_area_1.LayerComponentDiagramBorderElement.TOP_TO_LEFT];
        this.borderSvg[layer_component_diagram_area_1.LayerComponentDiagramBorderElement.TOP_TO_RIGHT] = "l50,0 l0,50";
        this.borderSvg[layer_component_diagram_area_1.LayerComponentDiagramBorderElement.TOP_TO_RIGHT_ROUND] = "l25,0 a25,25 0 0,1 25,25 l0,25";
        this.borderSvg[layer_component_diagram_area_1.LayerComponentDiagramBorderElement.RIGHT_TO_TOP] = "l0,-50 l-50,0";
        this.borderSvg[layer_component_diagram_area_1.LayerComponentDiagramBorderElement.RIGHT_TO_TOP_ROUND] = this.borderSvg[layer_component_diagram_area_1.LayerComponentDiagramBorderElement.RIGHT_TO_TOP];
        this.borderSvg[layer_component_diagram_area_1.LayerComponentDiagramBorderElement.BOTTOM_TO_LEFT] = "l-50,0 l0,-50";
        this.borderSvg[layer_component_diagram_area_1.LayerComponentDiagramBorderElement.BOTTOM_TO_LEFT_ROUND] = "l-25,0 a-25,-25 0 0,1 -25,-25 l0,-25";
        this.borderSvg[layer_component_diagram_area_1.LayerComponentDiagramBorderElement.LEFT_TO_BOTTOM] = "l0,50 l50,0";
        this.borderSvg[layer_component_diagram_area_1.LayerComponentDiagramBorderElement.LEFT_TO_BOTTOM_ROUND] = this.borderSvg[layer_component_diagram_area_1.LayerComponentDiagramBorderElement.LEFT_TO_BOTTOM];
        this.borderSvg[layer_component_diagram_area_1.LayerComponentDiagramBorderElement.RIGHT_TO_BOTTOM] = "l0,50 l-50,0";
        this.borderSvg[layer_component_diagram_area_1.LayerComponentDiagramBorderElement.RIGHT_TO_BOTTOM_ROUND] = "l0,25 a-25,25 0 0,1 -25,25 l-25,0";
        this.borderSvg[layer_component_diagram_area_1.LayerComponentDiagramBorderElement.BOTTOM_TO_RIGHT] = "l50,0 l0,-50";
        this.borderSvg[layer_component_diagram_area_1.LayerComponentDiagramBorderElement.BOTTOM_TO_RIGHT_ROUND] = this.borderSvg[layer_component_diagram_area_1.LayerComponentDiagramBorderElement.BOTTOM_TO_RIGHT];
        this.borderSvg[layer_component_diagram_area_1.LayerComponentDiagramBorderElement.BORDER_LEFT] = "l-100,0";
        this.borderSvg[layer_component_diagram_area_1.LayerComponentDiagramBorderElement.BORDER_RIGHT] = "l100,0";
        this.borderSvg[layer_component_diagram_area_1.LayerComponentDiagramBorderElement.BORDER_UP] = "l0,-100";
        this.borderSvg[layer_component_diagram_area_1.LayerComponentDiagramBorderElement.BORDER_DOWN] = "l0,100";
        this.borderSvg[layer_component_diagram_area_1.LayerComponentDiagramBorderElement.INTERFACE_UP_RIGHT] = "c12.5,-50 87.5,-50 100,0";
        this.borderSvg[layer_component_diagram_area_1.LayerComponentDiagramBorderElement.INTERFACE_UP_LEFT] = "c-12.5,-50 -87.5,-50 -100,0";
        this.borderSvg[layer_component_diagram_area_1.LayerComponentDiagramBorderElement.INTERFACE_DOWN_RIGHT] = "c12.5,50 87.5,50 100,0";
        this.borderSvg[layer_component_diagram_area_1.LayerComponentDiagramBorderElement.INTERFACE_DOWN_LEFT] = "c-12.5,50 -87.5,50 -100,0";
        this.borderSvg[layer_component_diagram_area_1.LayerComponentDiagramBorderElement.INTERFACE_LEFT_DOWN] = "c-50,12.5 -50,87.5 0,100";
        this.borderSvg[layer_component_diagram_area_1.LayerComponentDiagramBorderElement.INTERFACE_LEFT_UP] = "c-50,-12.5 -50,-87.5 0,-100";
        this.borderSvg[layer_component_diagram_area_1.LayerComponentDiagramBorderElement.INTERFACE_RIGHT_DOWN] = "c50,12.5 50,87.5 0,100";
        this.borderSvg[layer_component_diagram_area_1.LayerComponentDiagramBorderElement.INTERFACE_RIGHT_UP] = "c50,-12.5 50,-87.5 0,-100";
    }
    createSvgTextElementString(node) {
        let sTextElements = "";
        let markers = node.getMarkers();
        markers.filter(marker => marker.properties).forEach(marker => {
            let x = node.getOffset().getX() + marker.getOffset().getX() + 1;
            let y = node.getOffset().getY() + marker.getOffset().getY() + 1;
            let properties = marker.properties;
            let textColor = properties.getProperty("text-color");
            let text = properties.getProperty("text");
            let className = properties.getProperty("class");
            let sTextElement = `<text x="${x * 100 + 50}" y="${y * 100 + 50}"`;
            if (textColor && textColor.length > 0)
                sTextElement += ` fill="${textColor}"`;
            if (className && className.length > 0)
                sTextElement += ` class="${className}"`;
            sTextElement += ">";
            if (text)
                sTextElement += text;
            sTextElement += "</text>";
            sTextElements = [sTextElements, sTextElement].join("\r\n");
        });
        return sTextElements;
    }
    createSvgPathElementString(node) {
        let svgPath = this.createPath(node);
        let sElement = '<path';
        let fill = "white";
        let stroke = "black";
        let strokeWidth = "5";
        let className = "layout-component-node";
        let markers = node.getMarkers();
        if (markers && markers.length > 0) {
            let marker = markers[0];
            let properties = marker.properties;
            if (properties) {
                let propertyFill = properties.getProperty("fill");
                if (propertyFill && propertyFill.length > 0)
                    fill = propertyFill;
                let propertyStroke = properties.getProperty("stroke");
                if (propertyStroke && propertyStroke.length > 0)
                    stroke = propertyStroke;
                let propertyStrokeWidth = properties.getProperty("stroke-width");
                if (propertyStrokeWidth && propertyStrokeWidth.length > 0)
                    strokeWidth = propertyStrokeWidth;
                let propertyClassName = properties.getProperty("class");
                if (propertyClassName && propertyClassName.length > 0)
                    className = propertyClassName;
            }
        }
        if (fill && fill.length > 0)
            sElement += ` fill="${fill}"`;
        if (stroke && stroke.length > 0)
            sElement += ` stroke="${stroke}"`;
        if (strokeWidth && strokeWidth.length > 0)
            sElement += ` stroke-width="${strokeWidth}"`;
        if (className && className.length > 0)
            sElement += ` class="${className}"`;
        sElement += ` d="${svgPath}"`;
        sElement += ' />';
        return sElement;
    }
    createPath(node) {
        let topLeftCorner = node.findTopLeftCorner();
        if (!topLeftCorner)
            throw new Error("No top left corner found for node");
        let startX = node.getOffset().getX() + topLeftCorner.getX() + 3;
        let startY = node.getOffset().getY() + topLeftCorner.getY() + 1;
        let svgPath = `M${startX * 100 + 50},${startY * 100 + 50}`;
        let first = true;
        let traversedBorderElementObserver = (borderElement) => {
            if (first) {
                first = false;
                return;
            }
            let svgPart = this.borderSvg[borderElement];
            svgPath = [svgPath, svgPart].join(" ");
        };
        let borderTraverser = new layer_component_diagram_area_1.LayerComponentDiagramAreaBorderTraverser(node, traversedBorderElementObserver);
        borderTraverser.traverseAndGetBorder(topLeftCorner);
        return svgPath;
    }
}
exports.LayerComponentDiagramNodeSvgPathBuilder = LayerComponentDiagramNodeSvgPathBuilder;
//# sourceMappingURL=layer.component.diagram.node.svg.builder.js.map