"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.LayerComponentDiagramNode = void 0;
const diagram_node_position_1 = require("./diagram.node.position");
const layer_component_diagram_area_1 = require("./layer.component.diagram.area");
class LayerComponentDiagramNode extends layer_component_diagram_area_1.LayerComponentDiagramArea {
    constructor(offset, borderSymbols, markers = []) {
        super(borderSymbols.map(borderSymbolsLine => borderSymbolsLine.join("")).join("\r\n"));
        this.offset = offset;
        this.borderSymbols = borderSymbols;
        this.markers = markers;
    }
    getOffset() {
        return this.offset;
    }
    getMarkers() {
        return this.markers;
    }
    static ofPattern(borderSymbols, markers = []) {
        let minBorderX = LayerComponentDiagramNode.getMinBorderX(borderSymbols);
        let maxBorderX = LayerComponentDiagramNode.getMaxBorderX(borderSymbols);
        let minBorderY = LayerComponentDiagramNode.getMinBorderY(borderSymbols);
        let maxBorderY = LayerComponentDiagramNode.getMaxBorderY(borderSymbols);
        if (maxBorderY > 0) {
            while (borderSymbols.length > maxBorderY + 1) {
                borderSymbols.pop();
            }
        }
        if (maxBorderX > 0) {
            for (let y = 0; y < borderSymbols.length; y++) {
                let borderSymbolsLine = borderSymbols[y];
                while (borderSymbolsLine.length > maxBorderX + 1) {
                    borderSymbolsLine.pop();
                }
            }
        }
        if (minBorderY > 0) {
            for (let i = 0; i < minBorderY; i++) {
                borderSymbols.shift();
            }
        }
        if (minBorderX > 0) {
            for (let y = 0; y < borderSymbols.length; y++) {
                let borderSymbolsLine = borderSymbols[y];
                for (let i = 0; i < minBorderX; i++) {
                    borderSymbolsLine.shift();
                }
            }
        }
        let offset = diagram_node_position_1.DiagramNodePosition.of(minBorderX, minBorderY);
        markers = markers.map(marker => layer_component_diagram_area_1.LayerComponentDiagramMarker.ofOffsetAndName(marker.getOffset().substractOffset(offset), marker.getName()));
        return new LayerComponentDiagramNode(offset, borderSymbols, markers);
    }
    static ofOffsetAndPattern(offset, borderSymbols, markers = []) {
        return new LayerComponentDiagramNode(offset, borderSymbols, markers);
    }
    static getMinBorderX(borderSymbols) {
        let minIndex = -1;
        for (let y = 0; y < borderSymbols.length; y++) {
            let borderSymbolsLine = borderSymbols[y];
            for (let x = 0; x < borderSymbolsLine.length; x++) {
                let symbol = borderSymbolsLine[x];
                if (symbol != ' ') {
                    minIndex = minIndex == -1 ? x : Math.min(minIndex, x);
                    break;
                }
            }
        }
        return minIndex;
    }
    static getMaxBorderX(borderSymbols) {
        let maxIndex = 0;
        for (let y = 0; y < borderSymbols.length; y++) {
            let borderSymbolsLine = borderSymbols[y];
            for (let x = borderSymbolsLine.length - 1; x >= 0; x--) {
                let symbol = borderSymbolsLine[x];
                if (symbol != ' ') {
                    maxIndex = Math.max(maxIndex, x);
                    break;
                }
            }
        }
        return maxIndex;
    }
    static getMinBorderY(borderSymbols) {
        let minIndex = -1;
        for (let y = 0; y < borderSymbols.length; y++) {
            let borderSymbolsLine = borderSymbols[y];
            for (let x = 0; x < borderSymbolsLine.length; x++) {
                let symbol = borderSymbolsLine[x];
                if (symbol != ' ') {
                    minIndex = minIndex == -1 ? y : Math.min(minIndex, y);
                    break;
                }
            }
        }
        return minIndex;
    }
    static getMaxBorderY(borderSymbols) {
        let minIndex = 0;
        for (let y = borderSymbols.length - 1; y >= 0; y--) {
            let borderSymbolsLine = borderSymbols[y];
            for (let x = 0; x < borderSymbolsLine.length; x++) {
                let symbol = borderSymbolsLine[x];
                if (symbol != ' ') {
                    minIndex = Math.max(minIndex, y);
                    break;
                }
            }
        }
        return minIndex;
    }
}
exports.LayerComponentDiagramNode = LayerComponentDiagramNode;
//# sourceMappingURL=layer.component.diagram.node.js.map