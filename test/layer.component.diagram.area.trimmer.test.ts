import {describe} from "mocha";
import {expect} from "chai";
import {LayerComponentDiagramAreaTrimmer} from "../layer.component.diagram.node";

describe('Trim', () => {
    const trimmer = new LayerComponentDiagramAreaTrimmer();
    const input = [
        [" ", " ", " ", " ", " "],
        [" ", " ", " ", " ", " "],
        [" ", "+", "+", "+", " "],
        [" ", "+", " ", "+", " "],
        [" ", "+", "+", "+", " "],
        [" ", " ", " ", " ", " "],
        [" ", " ", " ", " ", " "]
    ];
    const expected = [
        ["+", "+", "+"],
        ["+", " ", "+"],
        ["+", "+", "+"]
    ];
    const actual = trimmer.trim(input);
    it('All sides', () => {
        expect(expected).to.equal(actual);
    });
});