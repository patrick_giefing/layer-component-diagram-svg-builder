"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mocha_1 = require("mocha");
const diagram_test_model_1 = require("./diagram.test.model");
const chai_1 = require("chai");
const layer_component_diagram_area_1 = require("../layer.component.diagram.area");
mocha_1.describe('Diagram node properties reader test', () => {
    it('All allowed variants', () => {
        let markerProperties = layer_component_diagram_area_1.LayerComponentDiagramMarkerPropertiesReader.readString(diagram_test_model_1.DiagramTestModel.testModelAllAllowedObjects);
        let propertiesA = markerProperties["A"];
        chai_1.expect(propertiesA).to.not.equals(null);
    });
});
//# sourceMappingURL=layer.component.diagram.marker.properties.reader.test.js.map