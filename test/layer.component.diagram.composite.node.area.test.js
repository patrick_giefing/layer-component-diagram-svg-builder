"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const layer_component_diagram_reader_1 = require("../layer.component.diagram.reader");
const layer_component_diagram_composite_node_area_1 = require("../layer.component.diagram.composite.node.area");
const diagram_test_model_1 = require("./diagram.test.model");
const chai_1 = require("chai");
const mocha_1 = require("mocha");
mocha_1.describe('Bottom with rounded edges', () => {
    mocha_1.describe('Nodes', () => {
        let representation = new layer_component_diagram_composite_node_area_1.LayerComponentDiagramCompositeNodeArea(diagram_test_model_1.DiagramTestModel.testModelBottomWithRoundedEdges);
        let diagram = layer_component_diagram_reader_1.LayerComponentDiagramReader.readFromRepresentation(representation);
        it('Count', () => {
            chai_1.expect(diagram.getNodes().length).to.equal(1);
        });
    });
});
mocha_1.describe('All allowed objects', () => {
    mocha_1.describe('Nodes', () => {
        let representation = new layer_component_diagram_composite_node_area_1.LayerComponentDiagramCompositeNodeArea(diagram_test_model_1.DiagramTestModel.testModelAllAllowedObjects);
        let diagram = layer_component_diagram_reader_1.LayerComponentDiagramReader.readFromRepresentation(representation);
        it('Count', () => {
            chai_1.expect(diagram.getNodes().length).to.equal(36);
        });
        let node0 = diagram.getNodes()[0];
        it('Order', () => {
            chai_1.expect(node0).to.not.equal(null);
            chai_1.expect(node0.getOffset()).to.not.equal(null);
            chai_1.expect(node0.getOffset().getX()).to.equal(1);
            chai_1.expect(node0.getOffset().getY()).to.equal(1);
        });
        it('markers', () => {
            chai_1.expect(node0).to.not.equal(null);
            let markers = node0.getMarkers();
            chai_1.expect(markers).to.not.equal(null);
            chai_1.expect(markers.length).to.equal(1);
        });
    });
});
//# sourceMappingURL=layer.component.diagram.composite.node.area.test.js.map