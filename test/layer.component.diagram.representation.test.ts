import {DiagramTestModel} from "./diagram.test.model";
import {expect} from 'chai';
import {describe} from 'mocha';
import {DiagramNodePosition} from "../diagram.node.position";
import {LayerComponentDiagramCompositeNodeArea} from "../layer.component.diagram.composite.node.area";

describe('StringRepresentation', () => {
    const model = new LayerComponentDiagramCompositeNodeArea(DiagramTestModel.testModelAllAllowedObjects);
    it('Create an check if dimensions available', () => {
        expect(model.getWidth()).to.greaterThan(0);
        expect(model.getHeight()).to.greaterThan(0);
    });
    it('Check symbols at position', () => {
        expect(model.getSymbolAt(DiagramNodePosition.of(1, 1))).to.eq("°");
        expect(model.getSymbolAt(DiagramNodePosition.of(-1, 1))).to.eq(null);
        expect(model.getSymbolAt(DiagramNodePosition.of(1, null))).to.eq(null);
    });
    it('Check top left corner', () => {
        expect(model.isTopLeftCorner(DiagramNodePosition.of(1, 1))).to.eq(true);
        expect(model.isTopLeftCorner(DiagramNodePosition.of(1, 2))).to.eq(false);
    });
    it('Check border', () => {
        expect(model.isBorderLeft(DiagramNodePosition.of(1, 1))).to.eq(false);
        expect(model.isBorderUp(DiagramNodePosition.of(1, 1))).to.eq(false);
        expect(model.isBorderRight(DiagramNodePosition.of(1, 1))).to.eq(true);
        expect(model.isBorderDown(DiagramNodePosition.of(1, 1))).to.eq(true);
    });
});