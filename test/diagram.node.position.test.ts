import {DiagramNodePosition} from '../diagram.node.position';
import {expect} from 'chai';
import {describe} from 'mocha';

describe('Position', () => {
    it('adding offset', () => {
        const pWithOffset = DiagramNodePosition.of(3, 7).addOffset(DiagramNodePosition.of(5, -2));
        expect(pWithOffset.getX()).to.equal(8);
        expect(pWithOffset.getY()).to.equal(5);
    });
});