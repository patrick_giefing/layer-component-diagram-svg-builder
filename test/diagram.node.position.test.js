"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const diagram_node_position_1 = require("../diagram.node.position");
const chai_1 = require("chai");
const mocha_1 = require("mocha");
mocha_1.describe('Position', () => {
    it('adding offset', () => {
        let pWithOffset = diagram_node_position_1.DiagramNodePosition.of(3, 7).addOffset(diagram_node_position_1.DiagramNodePosition.of(5, -2));
        chai_1.expect(pWithOffset.getX()).to.equal(8);
        chai_1.expect(pWithOffset.getY()).to.equal(5);
    });
});
//# sourceMappingURL=diagram.node.position.test.js.map