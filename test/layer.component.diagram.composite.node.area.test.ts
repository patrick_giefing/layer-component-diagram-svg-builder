import {LayerComponentDiagramReader} from "../layer.component.diagram.reader";
import {LayerComponentDiagramCompositeNodeArea} from "../layer.component.diagram.composite.node.area";
import {DiagramTestModel} from "./diagram.test.model";
import {expect} from 'chai';
import {describe} from 'mocha';

describe('Bottom with rounded edges', () => {
    describe('Nodes', () => {
        const representation = new LayerComponentDiagramCompositeNodeArea(DiagramTestModel.testModelBottomWithRoundedEdges);
        const diagram = LayerComponentDiagramReader.readFromRepresentation(representation);
        it('Count', () => {
            expect(diagram.getNodes().length).to.equal(1);
        });
    });
});

describe('All allowed objects', () => {
    describe('Nodes', () => {
        const representation = new LayerComponentDiagramCompositeNodeArea(DiagramTestModel.testModelAllAllowedObjects);
        const diagram = LayerComponentDiagramReader.readFromRepresentation(representation);
        it('Count', () => {
            expect(diagram.getNodes().length).to.equal(36);
        });
        const node0 = diagram.getNodes()[0];
        it('Order', () => {
            expect(node0).to.not.equal(null);
            expect(node0.getOffset()).to.not.equal(null);
            expect(node0.getOffset().getX()).to.equal(1);
            expect(node0.getOffset().getY()).to.equal(1);
        });
        it('markers', () => {
            expect(node0).to.not.equal(null);
            const markers = node0.getMarkers();
            expect(markers).to.not.equal(null);
            expect(markers.length).to.equal(1);
        });
    });
});