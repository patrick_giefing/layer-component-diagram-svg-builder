"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const diagram_test_model_1 = require("./diagram.test.model");
const chai_1 = require("chai");
const mocha_1 = require("mocha");
const diagram_node_position_1 = require("../diagram.node.position");
const layer_component_diagram_composite_node_area_1 = require("../layer.component.diagram.composite.node.area");
mocha_1.describe('StringRepresentation', () => {
    let model = new layer_component_diagram_composite_node_area_1.LayerComponentDiagramCompositeNodeArea(diagram_test_model_1.DiagramTestModel.testModelAllAllowedObjects);
    it('Create an check if dimensions available', () => {
        chai_1.expect(model.getWidth()).to.greaterThan(0);
        chai_1.expect(model.getHeight()).to.greaterThan(0);
    });
    it('Check symbols at position', () => {
        chai_1.expect(model.getSymbolAt(diagram_node_position_1.DiagramNodePosition.of(1, 1))).to.eq("°");
        chai_1.expect(model.getSymbolAt(diagram_node_position_1.DiagramNodePosition.of(-1, 1))).to.eq(null);
        chai_1.expect(model.getSymbolAt(diagram_node_position_1.DiagramNodePosition.of(1, null))).to.eq(null);
    });
    it('Check top left corner', () => {
        chai_1.expect(model.isTopLeftCorner(diagram_node_position_1.DiagramNodePosition.of(1, 1))).to.eq(true);
        chai_1.expect(model.isTopLeftCorner(diagram_node_position_1.DiagramNodePosition.of(1, 2))).to.eq(false);
    });
    it('Check border', () => {
        chai_1.expect(model.isBorderLeft(diagram_node_position_1.DiagramNodePosition.of(1, 1))).to.eq(false);
        chai_1.expect(model.isBorderUp(diagram_node_position_1.DiagramNodePosition.of(1, 1))).to.eq(false);
        chai_1.expect(model.isBorderRight(diagram_node_position_1.DiagramNodePosition.of(1, 1))).to.eq(true);
        chai_1.expect(model.isBorderDown(diagram_node_position_1.DiagramNodePosition.of(1, 1))).to.eq(true);
    });
});
//# sourceMappingURL=layer.component.diagram.representation.test.js.map