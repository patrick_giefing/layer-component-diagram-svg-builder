"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DiagramTestModel = void 0;
class DiagramTestModel {
}
exports.DiagramTestModel = DiagramTestModel;
DiagramTestModel.testModelBottomWithRoundedEdges = `+^++
+  +
°++°`;
DiagramTestModel.testModelAllAllowedObjects = `
 °++++++° °+++++++++++++++++++° °+++++++++++°
 + [A]  + + [E]   + [F]   +   + +[K]<   +   +
 ++^+++++ +   ++^+++^++   +[J]+ ++^++[M]+[N]+
 + [B]  + +   +  [G]  +   +   + +[L]+   <   +
 ++^+++++ ++^+++^+++++++^+++^++ ++^+++^+++^++
 + [C]  + +      [H]          + +    [O]    +
 ++^+++++ ++^++++++++++++++++++ ++^+++^+++^++
 + [D]  + +      [I]          + +    [P]    +
 °++++++° °+++++++++++++++++++° °+++++++++++°

 °+++++++++++++++++++° °++++++++++++++++++++°
 +    +[R] +    +    + +      +[Z] +  [1]   +
 +[Q] +^++++[T] +[U] + +      +^++++^++++^+++
 +    +[S] +    +    + +       [Y]      +[2]+
 +^++++^++++^++++^++++ + °++++++++++++° ++^++
 +[V] >     [W]      + + +[3]<[4]>[5] +     +
 +^++++^++++^+++++++++ + ++^+++v+++^+++     +
 +         +    [9]  + + +[6]+[7]+[8] +     +
 +         +^++++^++++ + °++++++++++++°     +
 +        [X]   +[0] + +                    +
 °+++++++++++++++++++° °++++++++++++++++++++°
          
 [A:text=Layer D|fill=#e0bbe4|stroke=black|stroke-width=5|class=module1]
 [B:text=Layer C|fill=#957dad|stroke=black|stroke-width=5|class=module1]
 [C:text=Layer B|fill=#d291bc|stroke=black|stroke-width=5|class=module1]
 [D:text=Layer A|fill=#fec8d8|stroke=black|stroke-width=5|class=module1]
 
 [E:text=Text|fill=#b29db6|stroke=black|stroke-width=5]
 [F:text=Text|fill=#d6d6d6|stroke=black|stroke-width=5]
 [G:text=Text|fill=#abc3ce|stroke=black|stroke-width=5]
 [H:text=Text|fill=#c5aeb4|stroke=black|stroke-width=5]
 [I:text=Text|fill=#7b8fa5|stroke=black|stroke-width=5]
 [J:text=Text|fill=#99b49f|stroke=black|stroke-width=5]
 
 [K:text=Text|fill=#efb0c9|stroke=black|stroke-width=5]
 [L:text=Text|fill=#f4c2d7|stroke=black|stroke-width=5]
 [M:text=Text|fill=#f8dae9|stroke=black|stroke-width=5]
 [N:text=Text|fill=#b9d6f3|stroke=black|stroke-width=5]
 [O:text=Text|fill=#a1c9f1|stroke=black|stroke-width=5]
 [P:text=Text|fill=#f1e8d9|stroke=black|stroke-width=5]
 
 [Q:text=Text|fill=#e5e28b|stroke=black|stroke-width=5]
 [R:text=Text|fill=#a7db8c|stroke=black|stroke-width=5]
 [S:text=Text|fill=#b4a7eb|stroke=black|stroke-width=5]
 [T:text=Text|fill=#f3a5bc|stroke=black|stroke-width=5]
 [U:text=Text|fill=#a0d8e9|stroke=black|stroke-width=5]
 [V:text=Text|fill=#d4b1aa|stroke=black|stroke-width=5]
 [W:text=Text|fill=#c69ebe|stroke=black|stroke-width=5]
 [X:text=Text|fill=#999acf|stroke=black|stroke-width=5]
 [9:text=Text|fill=#efb0c9|stroke=black|stroke-width=5]
 [0:text=Text|fill=#f4c2d7|stroke=black|stroke-width=5]
 
 [Z:text=Text|fill=#afd6f7|stroke=black|stroke-width=5]
 [Y:text=Text|fill=#b4ceb3|stroke=black|stroke-width=5]
 [1:text=Text|fill=#e4d8b9|stroke=black|stroke-width=5]
 [2:text=Text|fill=#dfbeaf|stroke=black|stroke-width=5]
 [3:text=Text|fill=#c69ebe|stroke=red|stroke-width=5]
 [4:text=Text|fill=#999acf|stroke=red|stroke-width=5]
 [5:text=Text|fill=#b29db6|stroke=red|stroke-width=5]
 [6:text=Text|fill=#d6d6d6|stroke=red|stroke-width=5]
 [7:text=Text|fill=#abc3ce|stroke=red|stroke-width=5]
 [8:text=Text|fill=#c5aeb4|stroke=red|stroke-width=5]
`;
//# sourceMappingURL=diagram.test.model.js.map