import {LayerComponentDiagramNodeSvgPathBuilder} from "../layer.component.diagram.node.svg.builder";
import {describe} from "mocha";
import {expect} from "chai";
import {DiagramTestModel} from "./diagram.test.model";
import {LayerComponentDiagramReader} from "../layer.component.diagram.reader";
import {LayerComponentDiagramCompositeNodeArea} from "../layer.component.diagram.composite.node.area";
import {LayerComponentDiagramSvgStringBuilder} from "../layer.component.diagram.svg.builder";

describe('Node SVG', () => {
    it('Node bottom with rounded edges', () => {
        const representation = new LayerComponentDiagramCompositeNodeArea(DiagramTestModel.testModelBottomWithRoundedEdges);
        const diagram = LayerComponentDiagramReader.readFromRepresentation(representation);
        const nodes = diagram.getNodes()
        expect(nodes.length).to.equal(1);
        const node = nodes[0];
        const nodeSvgPathBuilder = new LayerComponentDiagramNodeSvgPathBuilder();
        const svgPath = nodeSvgPathBuilder.createPath(node);
        expect(svgPath).to.equal("M350,150 l100,0 l50,0 l0,50 l0,100 l0,25 a-25,25 0 0,1 -25,25 l-25,0 l-100,0 l-100,0 l-25,0 a-25,-25 0 0,1 -25,-25 l0,-25 l0,-100 l0,-50 l50,0 c12.5,-50 87.5,-50 100,0");
    });
    it('Model all allowed objects', () => {
        const svgStringBuilder = new LayerComponentDiagramSvgStringBuilder();
        const svgString = svgStringBuilder.createSvgElementStringFromString(DiagramTestModel.testModelAllAllowedObjects);
        // just a dirty "test" for a console output because the test-SVG has all possible elements combined and
        // I needed visual confirmation - yeah - I know
        console.log(svgString);
    });
});