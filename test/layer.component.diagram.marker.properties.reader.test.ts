import {describe} from "mocha";
import {DiagramTestModel} from "./diagram.test.model";
import {expect} from "chai";
import {LayerComponentDiagramMarkerPropertiesReader} from "../layer.component.diagram.area";

describe('Diagram node properties reader test', () => {
    it('All allowed variants', () => {
        const markerProperties = LayerComponentDiagramMarkerPropertiesReader.readString(DiagramTestModel.testModelAllAllowedObjects);
        const propertiesA = markerProperties["A"];
        expect(propertiesA).to.not.equals(null);
    });
});