"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const layer_component_diagram_node_svg_builder_1 = require("../layer.component.diagram.node.svg.builder");
const mocha_1 = require("mocha");
const chai_1 = require("chai");
const diagram_test_model_1 = require("./diagram.test.model");
const layer_component_diagram_reader_1 = require("../layer.component.diagram.reader");
const layer_component_diagram_composite_node_area_1 = require("../layer.component.diagram.composite.node.area");
const layer_component_diagram_svg_builder_1 = require("../layer.component.diagram.svg.builder");
mocha_1.describe('Node SVG', () => {
    it('Node bottom with rounded edges', () => {
        let representation = new layer_component_diagram_composite_node_area_1.LayerComponentDiagramCompositeNodeArea(diagram_test_model_1.DiagramTestModel.testModelBottomWithRoundedEdges);
        let diagram = layer_component_diagram_reader_1.LayerComponentDiagramReader.readFromRepresentation(representation);
        let nodes = diagram.getNodes();
        chai_1.expect(nodes.length).to.equal(1);
        let node = nodes[0];
        let nodeSvgPathBuilder = new layer_component_diagram_node_svg_builder_1.LayerComponentDiagramNodeSvgPathBuilder();
        let svgPath = nodeSvgPathBuilder.createPath(node);
        chai_1.expect(svgPath).to.equal("M350,150 l100,0 l50,0 l0,50 l0,100 l0,25 a-25,25 0 0,1 -25,25 l-25,0 l-100,0 l-100,0 l-25,0 a-25,-25 0 0,1 -25,-25 l0,-25 l0,-100 l0,-50 l50,0 c12.5,-50 87.5,-50 100,0");
    });
    it('Model all allowed objects', () => {
        let svgStringBuilder = new layer_component_diagram_svg_builder_1.LayerComponentDiagramSvgStringBuilder();
        let svgString = svgStringBuilder.createSvgElementStringFromString(diagram_test_model_1.DiagramTestModel.testModelAllAllowedObjects);
        // just a dirty "test" for a console output because the test-SVG has all possible elements combined and
        // I needed visual confirmation - yeah - I know
        console.log(svgString);
    });
});
//# sourceMappingURL=layer.component.diagram.node.svg.builder.test.js.map