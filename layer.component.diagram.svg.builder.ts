import {LayerComponentDiagramCompositeNodeArea} from "./layer.component.diagram.composite.node.area";
import {LayerComponentDiagramReader} from "./layer.component.diagram.reader";
import {LayerComponentDiagramNodeSvgPathBuilder} from "./layer.component.diagram.node.svg.builder";

export class LayerComponentDiagramSvgStringBuilder {
    public createSvgElementStringFromString(s: string): string {
        const representation = new LayerComponentDiagramCompositeNodeArea(s);
        const svgContentString = this.createSvgContentElementsString(representation);
        const width = (representation.getWidth() + 100) * 100;
        const height = (representation.getHeight() + 100) * 100;
        const svgElementString = `<svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 ${width} ${height}">${svgContentString}</svg>`;
        return svgElementString;
    }

    public createSvgContentElementsStringFromString(s: string): string {
        const representation = new LayerComponentDiagramCompositeNodeArea(s);
        const svgContentString = this.createSvgContentElementsString(representation);
        return svgContentString;
    }

    private createSvgContentElementsString(representation: LayerComponentDiagramCompositeNodeArea): string {
        const diagram = LayerComponentDiagramReader.readFromRepresentation(representation);
        const nodes = diagram.getNodes()
        const nodeSvgPathBuilder = new LayerComponentDiagramNodeSvgPathBuilder();
        let svgContentString = "";
        nodes.forEach(node => {
            const svgPathElementString = nodeSvgPathBuilder.createSvgPathElementString(node);
            svgContentString += svgPathElementString + "\r\n";
        });
        nodes.forEach(node => {
            const svgTextElementString = nodeSvgPathBuilder.createSvgTextElementString(node);
            if (svgTextElementString)
                svgContentString += svgTextElementString + "\r\n";
        });
        return svgContentString;
    }
}
