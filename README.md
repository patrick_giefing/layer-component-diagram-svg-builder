# SVG layout component diagram builder

Creates SVG paths for a kind of structure diagram.

[sample.html](./test/sample.html)

```javascript
function generateSvg() {
    var svgContent = diagramSvgBuilder.createSvgContentElementsStringFromString(txtExample.value);
    var svg = '<svg xmlns="http://www.w3.org/2000/svg" version="1.1" viewBox="0 0 5500 2200" width="30cm" height="15cm">\n' +
        '  <title>Sample SVG output</title>\n' +
        '  <desc></desc>\n' +
        '  <style type="text/css"><![CDATA[\n' +
        '    .Border { fill:none; stroke:blue; stroke-width:1 }\n' +
        '    text.module1 {font-weight: bold;fill:brown;}\n' +
        '    text.h1 {font-weight: bold;font-size:108px;}\n' +
        '\ttext {font-size: 72px;}\n' +
        '  ]]></style>' + svgContent + '</svg>';
    svgContainer.innerHTML = svg;
}
```

**Input:**

```ini
 °++++++° °+++++++++++++++++++° °+++++++++++°
 + [A]  + + [E]   + [F]   +   + +[K]<   +   +
 ++^+++++ +   ++^+++^++   +[J]+ ++^++[M]+[N]+
 + [B]  + +   +  [G]  +   +   + +[L]+   <   +
 ++^+++++ ++^+++^+++++++^+++^++ ++^+++^+++^++
 + [C]  + +      [H]          + +    [O]    +
 ++^+++++ ++^++++++++++++++++++ ++^+++^+++^++
 + [D]  + +      [I]          + +    [P]    +
 °++++++° °+++++++++++++++++++° °+++++++++++°

 °+++++++++++++++++++° °++++++++++++++++++++°
 +    +[R] +    +    + +      +[Z] +  [1]   +
 +[Q] +^++++[T] +[U] + +      +^++++^++++^+++
 +    +[S] +    +    + +                +[2]+
 +^++++^++++^++++^++++ + °++++++++++++° ++^++
 +[V] >     [W]      + + +[3]<[4]>[5] +     +
 +^++++^++++^+++++++++ + ++^+++v+++^+++     +
 + [X1]    +    [9]  + + +[6]+[7]+[8] +     +
 + [X2]    +^++++^++++ + °++++++++++++°     +
 + [X3]         +[0] + +  [Y]               +
 °+++++++++++++++++++° °++++++++++++++++++++°
          
 [A:text=Layer D|fill=#e0bbe4|stroke=black|stroke-width=5|class=module1]
 [B:text=Layer C|fill=#957dad|stroke=black|stroke-width=5|class=module1]
 [C:text=Layer B|fill=#d291bc|stroke=black|stroke-width=5|class=module1]
 [D:text=Layer A|fill=#fec8d8|stroke=black|stroke-width=5|class=module1]
 
 [E:text=E|fill=#b29db6|stroke=black|stroke-width=5]
 [F:text=F|fill=#d6d6d6|stroke=black|stroke-width=5]
 [G:text=G|fill=#abc3ce|stroke=black|stroke-width=5]
 [H:text=H|fill=#c5aeb4|stroke=black|stroke-width=5]
 [I:text=I|fill=#7b8fa5|stroke=black|stroke-width=5]
 [J:text=J|fill=#99b49f|stroke=black|stroke-width=5]
 
 [K:text=K|fill=#efb0c9|stroke=black|stroke-width=5]
 [L:text=L|fill=#f4c2d7|stroke=black|stroke-width=5]
 [M:text=M|fill=#f8dae9|stroke=black|stroke-width=5]
 [N:text=N|fill=#b9d6f3|stroke=black|stroke-width=5]
 [O:text=O|fill=#a1c9f1|stroke=black|stroke-width=5]
 [P:text=P|fill=#f1e8d9|stroke=black|stroke-width=5]
 
 [Q:text=O|fill=#e5e28b|stroke=black|stroke-width=5]
 [R:text=R|fill=#a7db8c|stroke=black|stroke-width=5]
 [S:text=S|fill=#b4a7eb|stroke=black|stroke-width=5]
 [T:text=T|fill=#f3a5bc|stroke=black|stroke-width=5]
 [U:text=U|fill=#a0d8e9|stroke=black|stroke-width=5]
 [V:text=V|fill=#d4b1aa|stroke=black|stroke-width=5]
 [W:text=W|fill=#c69ebe|stroke=black|stroke-width=5]
 [X1:text=A very|fill=#999acf|stroke=black|stroke-width=5|class=h1]
 [X2:text=important|fill=#999acf|stroke=black|stroke-width=5|class=h1]
 [X3:text=component|fill=#999acf|stroke=black|stroke-width=5|class=h1]
 [9:text=9|fill=#efb0c9|stroke=black|stroke-width=5]
 [0:text=0|fill=#f4c2d7|stroke=black|stroke-width=5]
 
 [Z:text=Z|fill=#afd6f7|stroke=black|stroke-width=5]
 [Y:text=Embedded Components|fill=#b4ceb3|stroke=black|stroke-width=5|class=h1]
 [1:text=1|fill=#e4d8b9|stroke=black|stroke-width=5]
 [2:text=2|fill=#dfbeaf|stroke=black|stroke-width=5]
 [3:text=Sub 1|fill=#c69ebe|stroke=red|stroke-width=5]
 [4:text=Sub 2|fill=#999acf|stroke=red|stroke-width=5]
 [5:text=Sub 3|fill=#b29db6|stroke=red|stroke-width=5]
 [6:text=Sub 4|fill=#d6d6d6|stroke=red|stroke-width=5]
 [7:text=Sub 5|fill=#abc3ce|stroke=red|stroke-width=5]
 [8:text=Sub 6|fill=#c5aeb4|stroke=red|stroke-width=5]
```

**Output:**

![alt text](./documentation/screenshots/sample-compound-diagram.png)
