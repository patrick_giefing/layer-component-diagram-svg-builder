"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.LayerComponentDiagramBorderElement = exports.LayerComponentDiagramAreaBorderTraverser = exports.LayerComponentDiagramMarkerPropertiesReader = exports.LayerComponentDiagramMarkerProperties = exports.LayerComponentDiagramMarker = exports.LayerComponentDiagramArea = void 0;
const diagram_node_position_1 = require("./diagram.node.position");
const layer_component_diagram_reader_1 = require("./layer.component.diagram.reader");
class LayerComponentDiagramArea {
    constructor(diagramString) {
        this.diagramString = diagramString;
        this.lines = diagramString.split(/\r?\n/g);
        this.lines = this.lines ? this.lines : [];
        this.height = this.lines.length;
        this.width = Math.max.apply(null, this.lines.map(line => line.length));
    }
    getSymbolAt(position) {
        let isValidPosition = this.isValidPosition(position);
        if (!isValidPosition)
            return null;
        let x = position.getX();
        let y = position.getY();
        let line = this.lines[y];
        if (!line || x >= line.length)
            return null;
        let symbol = line[x];
        return symbol;
    }
    getDiagramString() {
        return this.diagramString;
    }
    getWidth() {
        return this.width;
    }
    getHeight() {
        return this.height;
    }
    isValidPosition(position) {
        if (!position)
            return false;
        let x = position.getX();
        let y = position.getY();
        if (x == NaN || y == NaN)
            return false;
        if (x < 0 || y < 0)
            return false;
        let width = this.getWidth();
        let height = this.getHeight();
        if (x >= width || y >= height)
            return false;
        return true;
    }
    areBorderSymbolsAt(points) {
        for (let point of points) {
            let symbol = this.getSymbolAt(point);
            if (!LayerComponentDiagramArea.isBorderSymbol(symbol))
                return false;
        }
        return true;
    }
    isTopLeftCorner(position) {
        return this.areBorderSymbolsAt([position, position.addOffset(diagram_node_position_1.DiagramNodePosition.of(1, 0)), position.addOffset(diagram_node_position_1.DiagramNodePosition.of(0, 1))]);
    }
    isTopRightCorner(position) {
        return this.areBorderSymbolsAt([position, position.addOffset(diagram_node_position_1.DiagramNodePosition.of(-1, 0)), position.addOffset(diagram_node_position_1.DiagramNodePosition.of(0, 1))]);
    }
    isBottomLeftCorner(position) {
        return this.areBorderSymbolsAt([position, position.addOffset(diagram_node_position_1.DiagramNodePosition.of(1, 0)), position.addOffset(diagram_node_position_1.DiagramNodePosition.of(0, -1))]);
    }
    isBottomRightCorner(position) {
        return this.areBorderSymbolsAt([position, position.addOffset(diagram_node_position_1.DiagramNodePosition.of(-1, 0)), position.addOffset(diagram_node_position_1.DiagramNodePosition.of(0, -1))]);
    }
    isBorderRight(position) {
        return this.areBorderSymbolsAt([position, position.addOffset(diagram_node_position_1.DiagramNodePosition.of(1, 0))]);
    }
    isBorderDown(position) {
        return this.areBorderSymbolsAt([position, position.addOffset(diagram_node_position_1.DiagramNodePosition.of(0, 1))]);
    }
    isBorderLeft(position) {
        return this.areBorderSymbolsAt([position, position.addOffset(diagram_node_position_1.DiagramNodePosition.of(-1, 0))]);
    }
    isBorderUp(position) {
        return this.areBorderSymbolsAt([position, position.addOffset(diagram_node_position_1.DiagramNodePosition.of(0, -1))]);
    }
    static isBorderSymbol(symbol) {
        let isBorderSymbol = LayerComponentDiagramArea.borderSymbols.indexOf(symbol) >= 0;
        return isBorderSymbol;
    }
    static isContentStartMarkerSymbol(symbol) {
        return symbol == "[";
    }
    findTopLeftCorner() {
        let topLeftCorners = this.findTopLeftCorners();
        return topLeftCorners.length > 0 ? topLeftCorners[0] : null;
    }
    findTopLeftCorners() {
        let topLeftCornerPositions = [];
        let diagramWidth = this.getWidth();
        let diagramHeight = this.getHeight();
        let lastPossibleTopLeftCornerStartPositionX = diagramWidth - 2;
        let lastPossibleTopLeftCornerStartPositionY = diagramHeight - 2;
        for (let x = 0; x < lastPossibleTopLeftCornerStartPositionX; x++) {
            for (let y = 0; y < lastPossibleTopLeftCornerStartPositionY; y++) {
                let position = diagram_node_position_1.DiagramNodePosition.of(x, y);
                let isTopLeftCorner = this.isTopLeftCorner(position);
                if (isTopLeftCorner)
                    topLeftCornerPositions.push(position);
            }
        }
        return topLeftCornerPositions;
    }
}
exports.LayerComponentDiagramArea = LayerComponentDiagramArea;
LayerComponentDiagramArea.borderSymbols = ["+", "^", "<", ">", "v", "°"];
class LayerComponentDiagramMarker {
    constructor(offset, name) {
        this.offset = offset;
        this.name = name;
    }
    static ofOffsetAndName(offset, name) {
        return new LayerComponentDiagramMarker(offset, name);
    }
    getOffset() {
        return this.offset;
    }
    getName() {
        return this.name;
    }
}
exports.LayerComponentDiagramMarker = LayerComponentDiagramMarker;
// TODO Builder object
class LayerComponentDiagramMarkerProperties {
    constructor(name, properties) {
        this.name = name;
        this.properties = properties;
    }
    getName() {
        return this.name;
    }
    getProperties() {
        return this.properties;
    }
    getProperty(propertyName) {
        let properties = this.properties;
        if (!properties)
            return null;
        let propertyValue = properties[propertyName];
        return propertyValue;
    }
}
exports.LayerComponentDiagramMarkerProperties = LayerComponentDiagramMarkerProperties;
class LayerComponentDiagramMarkerPropertiesReader {
    static readString(s) {
        let markerProperties = {};
        let reg = new RegExp(/\[([A-Za-z0-9_-]+)\:([^\]]+)\]/g);
        let result;
        while (result = reg.exec(s)) {
            let name = result[1];
            var properties = {};
            let propertiesString = result[2];
            let propertiesParts = propertiesString.split("|");
            for (let propertyPart of propertiesParts) {
                let seperatorIndex = propertyPart.indexOf("=");
                if (seperatorIndex > 0) {
                    let propertyName = propertyPart.substring(0, seperatorIndex);
                    let propertyValue = propertyPart.substring(seperatorIndex + 1);
                    properties[propertyName] = propertyValue;
                }
            }
            markerProperties[name] = new LayerComponentDiagramMarkerProperties(name, properties);
        }
        return markerProperties;
    }
}
exports.LayerComponentDiagramMarkerPropertiesReader = LayerComponentDiagramMarkerPropertiesReader;
class LayerComponentDiagramAreaBorderTraverser {
    constructor(drawingArea, traversedBorderElementObserver = null) {
        this.drawingArea = drawingArea;
        this.traversedBorderElementObserver = traversedBorderElementObserver;
    }
    traverseAndGetBorder(startPosition) {
        let width = this.drawingArea.getWidth();
        let height = this.drawingArea.getHeight();
        let border = new Array(height).fill(false).map(() => new Array(width).fill(false));
        let borderSearchDirection = layer_component_diagram_reader_1.DiagramElementDirection.RIGHT;
        let position = startPosition.addOffset(diagram_node_position_1.DiagramNodePosition.of(1, 0));
        while (true) {
            let isBorderAtPosition = border[position.getY()][position.getX()];
            if (isBorderAtPosition)
                break;
            border[position.getY()][position.getX()] = true;
            if (borderSearchDirection == layer_component_diagram_reader_1.DiagramElementDirection.RIGHT) {
                if (this.drawingArea.isBorderDown(position)) {
                    this.fireTraversedBorderElementObserver(LayerComponentDiagramBorderElement.TOP_TO_RIGHT, position);
                    position = position.addOffset(diagram_node_position_1.DiagramNodePosition.of(0, 1));
                    borderSearchDirection = layer_component_diagram_reader_1.DiagramElementDirection.DOWN;
                }
                else if (this.drawingArea.isBorderRight(position)) {
                    this.fireTraversedBorderElementObserver(LayerComponentDiagramBorderElement.BORDER_RIGHT, position);
                    position = position.addOffset(diagram_node_position_1.DiagramNodePosition.of(1, 0));
                }
                else if (this.drawingArea.isBorderUp((position))) {
                    this.fireTraversedBorderElementObserver(LayerComponentDiagramBorderElement.BOTTOM_TO_RIGHT, position);
                    position = position.addOffset(diagram_node_position_1.DiagramNodePosition.of(0, -1));
                    borderSearchDirection = layer_component_diagram_reader_1.DiagramElementDirection.UP;
                }
                else
                    return null;
            }
            else if (borderSearchDirection == layer_component_diagram_reader_1.DiagramElementDirection.DOWN) {
                if (this.drawingArea.isBorderLeft(position)) {
                    this.fireTraversedBorderElementObserver(LayerComponentDiagramBorderElement.RIGHT_TO_BOTTOM, position);
                    position = position.addOffset(diagram_node_position_1.DiagramNodePosition.of(-1, 0));
                    borderSearchDirection = layer_component_diagram_reader_1.DiagramElementDirection.LEFT;
                }
                else if (this.drawingArea.isBorderDown(position)) {
                    this.fireTraversedBorderElementObserver(LayerComponentDiagramBorderElement.BORDER_DOWN, position);
                    position = position.addOffset(diagram_node_position_1.DiagramNodePosition.of(0, 1));
                }
                else if (this.drawingArea.isBorderRight(position)) {
                    this.fireTraversedBorderElementObserver(LayerComponentDiagramBorderElement.LEFT_TO_BOTTOM, position);
                    position = position.addOffset(diagram_node_position_1.DiagramNodePosition.of(1, 0));
                    borderSearchDirection = layer_component_diagram_reader_1.DiagramElementDirection.RIGHT;
                }
                else
                    return null;
            }
            else if (borderSearchDirection == layer_component_diagram_reader_1.DiagramElementDirection.LEFT) {
                if (this.drawingArea.isBorderUp(position)) {
                    this.fireTraversedBorderElementObserver(LayerComponentDiagramBorderElement.BOTTOM_TO_LEFT, position);
                    position = position.addOffset(diagram_node_position_1.DiagramNodePosition.of(0, -1));
                    borderSearchDirection = layer_component_diagram_reader_1.DiagramElementDirection.UP;
                }
                else if (this.drawingArea.isBorderLeft(position)) {
                    this.fireTraversedBorderElementObserver(LayerComponentDiagramBorderElement.BORDER_LEFT, position);
                    position = position.addOffset(diagram_node_position_1.DiagramNodePosition.of(-1, 0));
                }
                else if (this.drawingArea.isBorderDown(position)) {
                    this.fireTraversedBorderElementObserver(LayerComponentDiagramBorderElement.TOP_TO_LEFT, position);
                    position = position.addOffset(diagram_node_position_1.DiagramNodePosition.of(0, 1));
                    borderSearchDirection = layer_component_diagram_reader_1.DiagramElementDirection.DOWN;
                }
                else
                    return null;
            }
            else /* UP */ {
                if (this.drawingArea.isBorderRight(position)) {
                    this.fireTraversedBorderElementObserver(LayerComponentDiagramBorderElement.LEFT_TO_TOP, position);
                    position = position.addOffset(diagram_node_position_1.DiagramNodePosition.of(1, 0));
                    borderSearchDirection = layer_component_diagram_reader_1.DiagramElementDirection.RIGHT;
                }
                else if (this.drawingArea.isBorderUp(position)) {
                    this.fireTraversedBorderElementObserver(LayerComponentDiagramBorderElement.BORDER_UP, position);
                    position = position.addOffset(diagram_node_position_1.DiagramNodePosition.of(0, -1));
                }
                else if (this.drawingArea.isBorderLeft(position)) {
                    this.fireTraversedBorderElementObserver(LayerComponentDiagramBorderElement.RIGHT_TO_TOP, position);
                    position = position.addOffset(diagram_node_position_1.DiagramNodePosition.of(-1, 0));
                    borderSearchDirection = layer_component_diagram_reader_1.DiagramElementDirection.LEFT;
                }
                else
                    return null;
            }
        }
        this.fireTraversedBorderElementObserver(LayerComponentDiagramBorderElement.BORDER_RIGHT, position);
        return border;
    }
    fireTraversedBorderElementObserver(borderElement, position) {
        if (!this.traversedBorderElementObserver)
            return;
        let symbol = this.drawingArea.getSymbolAt(position);
        if (symbol == "^") {
            if (borderElement == LayerComponentDiagramBorderElement.BORDER_LEFT) {
                borderElement = LayerComponentDiagramBorderElement.INTERFACE_UP_LEFT;
            }
            else if (borderElement == LayerComponentDiagramBorderElement.BORDER_RIGHT) {
                borderElement = LayerComponentDiagramBorderElement.INTERFACE_UP_RIGHT;
            }
        }
        else if (symbol == "v") {
            if (borderElement == LayerComponentDiagramBorderElement.BORDER_LEFT) {
                borderElement = LayerComponentDiagramBorderElement.INTERFACE_DOWN_LEFT;
            }
            else if (borderElement == LayerComponentDiagramBorderElement.BORDER_RIGHT) {
                borderElement = LayerComponentDiagramBorderElement.INTERFACE_DOWN_RIGHT;
            }
        }
        else if (symbol == "<") {
            if (borderElement == LayerComponentDiagramBorderElement.BORDER_UP) {
                borderElement = LayerComponentDiagramBorderElement.INTERFACE_LEFT_UP;
            }
            else if (borderElement == LayerComponentDiagramBorderElement.BORDER_DOWN) {
                borderElement = LayerComponentDiagramBorderElement.INTERFACE_LEFT_DOWN;
            }
        }
        else if (symbol == ">") {
            if (borderElement == LayerComponentDiagramBorderElement.BORDER_UP) {
                borderElement = LayerComponentDiagramBorderElement.INTERFACE_RIGHT_UP;
            }
            else if (borderElement == LayerComponentDiagramBorderElement.BORDER_DOWN) {
                borderElement = LayerComponentDiagramBorderElement.INTERFACE_RIGHT_DOWN;
            }
        }
        else if (symbol == "°") {
            if (borderElement == LayerComponentDiagramBorderElement.LEFT_TO_TOP) {
                borderElement = LayerComponentDiagramBorderElement.LEFT_TO_TOP_ROUND;
            }
            else if (borderElement == LayerComponentDiagramBorderElement.RIGHT_TO_BOTTOM) {
                borderElement = LayerComponentDiagramBorderElement.RIGHT_TO_BOTTOM_ROUND;
            }
            else if (borderElement == LayerComponentDiagramBorderElement.TOP_TO_LEFT) {
                borderElement = LayerComponentDiagramBorderElement.TOP_TO_LEFT_ROUND;
            }
            else if (borderElement == LayerComponentDiagramBorderElement.BOTTOM_TO_RIGHT) {
                borderElement = LayerComponentDiagramBorderElement.BOTTOM_TO_RIGHT_ROUND;
            }
            else if (borderElement == LayerComponentDiagramBorderElement.TOP_TO_RIGHT) {
                borderElement = LayerComponentDiagramBorderElement.TOP_TO_RIGHT_ROUND;
            }
            else if (borderElement == LayerComponentDiagramBorderElement.BOTTOM_TO_LEFT) {
                borderElement = LayerComponentDiagramBorderElement.BOTTOM_TO_LEFT_ROUND;
            }
            else if (borderElement == LayerComponentDiagramBorderElement.RIGHT_TO_TOP) {
                borderElement = LayerComponentDiagramBorderElement.RIGHT_TO_TOP_ROUND;
            }
            else if (borderElement == LayerComponentDiagramBorderElement.LEFT_TO_BOTTOM) {
                borderElement = LayerComponentDiagramBorderElement.LEFT_TO_BOTTOM_ROUND;
            }
        }
        this.traversedBorderElementObserver(borderElement);
    }
}
exports.LayerComponentDiagramAreaBorderTraverser = LayerComponentDiagramAreaBorderTraverser;
var LayerComponentDiagramBorderElement;
(function (LayerComponentDiagramBorderElement) {
    LayerComponentDiagramBorderElement[LayerComponentDiagramBorderElement["TOP_TO_LEFT"] = 0] = "TOP_TO_LEFT";
    LayerComponentDiagramBorderElement[LayerComponentDiagramBorderElement["TOP_TO_RIGHT"] = 1] = "TOP_TO_RIGHT";
    LayerComponentDiagramBorderElement[LayerComponentDiagramBorderElement["BOTTOM_TO_LEFT"] = 2] = "BOTTOM_TO_LEFT";
    LayerComponentDiagramBorderElement[LayerComponentDiagramBorderElement["BOTTOM_TO_RIGHT"] = 3] = "BOTTOM_TO_RIGHT";
    LayerComponentDiagramBorderElement[LayerComponentDiagramBorderElement["TOP_TO_LEFT_ROUND"] = 4] = "TOP_TO_LEFT_ROUND";
    LayerComponentDiagramBorderElement[LayerComponentDiagramBorderElement["TOP_TO_RIGHT_ROUND"] = 5] = "TOP_TO_RIGHT_ROUND";
    LayerComponentDiagramBorderElement[LayerComponentDiagramBorderElement["BOTTOM_TO_LEFT_ROUND"] = 6] = "BOTTOM_TO_LEFT_ROUND";
    LayerComponentDiagramBorderElement[LayerComponentDiagramBorderElement["BOTTOM_TO_RIGHT_ROUND"] = 7] = "BOTTOM_TO_RIGHT_ROUND";
    LayerComponentDiagramBorderElement[LayerComponentDiagramBorderElement["LEFT_TO_TOP"] = 8] = "LEFT_TO_TOP";
    LayerComponentDiagramBorderElement[LayerComponentDiagramBorderElement["RIGHT_TO_TOP"] = 9] = "RIGHT_TO_TOP";
    LayerComponentDiagramBorderElement[LayerComponentDiagramBorderElement["LEFT_TO_BOTTOM"] = 10] = "LEFT_TO_BOTTOM";
    LayerComponentDiagramBorderElement[LayerComponentDiagramBorderElement["RIGHT_TO_BOTTOM"] = 11] = "RIGHT_TO_BOTTOM";
    LayerComponentDiagramBorderElement[LayerComponentDiagramBorderElement["LEFT_TO_TOP_ROUND"] = 12] = "LEFT_TO_TOP_ROUND";
    LayerComponentDiagramBorderElement[LayerComponentDiagramBorderElement["RIGHT_TO_TOP_ROUND"] = 13] = "RIGHT_TO_TOP_ROUND";
    LayerComponentDiagramBorderElement[LayerComponentDiagramBorderElement["LEFT_TO_BOTTOM_ROUND"] = 14] = "LEFT_TO_BOTTOM_ROUND";
    LayerComponentDiagramBorderElement[LayerComponentDiagramBorderElement["RIGHT_TO_BOTTOM_ROUND"] = 15] = "RIGHT_TO_BOTTOM_ROUND";
    LayerComponentDiagramBorderElement[LayerComponentDiagramBorderElement["BORDER_LEFT"] = 16] = "BORDER_LEFT";
    LayerComponentDiagramBorderElement[LayerComponentDiagramBorderElement["BORDER_RIGHT"] = 17] = "BORDER_RIGHT";
    LayerComponentDiagramBorderElement[LayerComponentDiagramBorderElement["BORDER_UP"] = 18] = "BORDER_UP";
    LayerComponentDiagramBorderElement[LayerComponentDiagramBorderElement["BORDER_DOWN"] = 19] = "BORDER_DOWN";
    LayerComponentDiagramBorderElement[LayerComponentDiagramBorderElement["INTERFACE_UP_LEFT"] = 20] = "INTERFACE_UP_LEFT";
    LayerComponentDiagramBorderElement[LayerComponentDiagramBorderElement["INTERFACE_UP_RIGHT"] = 21] = "INTERFACE_UP_RIGHT";
    LayerComponentDiagramBorderElement[LayerComponentDiagramBorderElement["INTERFACE_DOWN_LEFT"] = 22] = "INTERFACE_DOWN_LEFT";
    LayerComponentDiagramBorderElement[LayerComponentDiagramBorderElement["INTERFACE_DOWN_RIGHT"] = 23] = "INTERFACE_DOWN_RIGHT";
    LayerComponentDiagramBorderElement[LayerComponentDiagramBorderElement["INTERFACE_LEFT_UP"] = 24] = "INTERFACE_LEFT_UP";
    LayerComponentDiagramBorderElement[LayerComponentDiagramBorderElement["INTERFACE_LEFT_DOWN"] = 25] = "INTERFACE_LEFT_DOWN";
    LayerComponentDiagramBorderElement[LayerComponentDiagramBorderElement["INTERFACE_RIGHT_UP"] = 26] = "INTERFACE_RIGHT_UP";
    LayerComponentDiagramBorderElement[LayerComponentDiagramBorderElement["INTERFACE_RIGHT_DOWN"] = 27] = "INTERFACE_RIGHT_DOWN";
})(LayerComponentDiagramBorderElement = exports.LayerComponentDiagramBorderElement || (exports.LayerComponentDiagramBorderElement = {}));
//# sourceMappingURL=layer.component.diagram.area.js.map