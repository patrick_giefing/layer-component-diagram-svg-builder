import {LayerComponentDiagramNode} from "./layer.component.diagram.node";
import {
    LayerComponentDiagramAreaBorderTraverser,
    LayerComponentDiagramBorderElement
} from "./layer.component.diagram.area";

export class LayerComponentDiagramNodeSvgPathBuilder {
    public static readonly FIELD_SIZE = 100;
    public static readonly BORDER_PADDING = 50;
    private borderSvg: { [id: number]: string; } = {};

    constructor() {
        this.initSvgPaths();
    }

    private initSvgPaths(): void {
        this.borderSvg[LayerComponentDiagramBorderElement.LEFT_TO_TOP] = "l0,-50 l50,0";
        this.borderSvg[LayerComponentDiagramBorderElement.LEFT_TO_TOP_ROUND] = "l0,-25 a25,-25 0 0,1 25,-25 l25,0";

        this.borderSvg[LayerComponentDiagramBorderElement.TOP_TO_LEFT] = "l-50,0 l0,50";
        this.borderSvg[LayerComponentDiagramBorderElement.TOP_TO_LEFT_ROUND] = this.borderSvg[LayerComponentDiagramBorderElement.TOP_TO_LEFT];

        this.borderSvg[LayerComponentDiagramBorderElement.TOP_TO_RIGHT] = "l50,0 l0,50";
        this.borderSvg[LayerComponentDiagramBorderElement.TOP_TO_RIGHT_ROUND] = "l25,0 a25,25 0 0,1 25,25 l0,25";

        this.borderSvg[LayerComponentDiagramBorderElement.RIGHT_TO_TOP] = "l0,-50 l-50,0";
        this.borderSvg[LayerComponentDiagramBorderElement.RIGHT_TO_TOP_ROUND] = this.borderSvg[LayerComponentDiagramBorderElement.RIGHT_TO_TOP];

        this.borderSvg[LayerComponentDiagramBorderElement.BOTTOM_TO_LEFT] = "l-50,0 l0,-50";
        this.borderSvg[LayerComponentDiagramBorderElement.BOTTOM_TO_LEFT_ROUND] = "l-25,0 a-25,-25 0 0,1 -25,-25 l0,-25";

        this.borderSvg[LayerComponentDiagramBorderElement.LEFT_TO_BOTTOM] = "l0,50 l50,0";
        this.borderSvg[LayerComponentDiagramBorderElement.LEFT_TO_BOTTOM_ROUND] = this.borderSvg[LayerComponentDiagramBorderElement.LEFT_TO_BOTTOM];

        this.borderSvg[LayerComponentDiagramBorderElement.RIGHT_TO_BOTTOM] = "l0,50 l-50,0";
        this.borderSvg[LayerComponentDiagramBorderElement.RIGHT_TO_BOTTOM_ROUND] = "l0,25 a-25,25 0 0,1 -25,25 l-25,0";

        this.borderSvg[LayerComponentDiagramBorderElement.BOTTOM_TO_RIGHT] = "l50,0 l0,-50";
        this.borderSvg[LayerComponentDiagramBorderElement.BOTTOM_TO_RIGHT_ROUND] = this.borderSvg[LayerComponentDiagramBorderElement.BOTTOM_TO_RIGHT];

        this.borderSvg[LayerComponentDiagramBorderElement.BORDER_LEFT] = "l-100,0";
        this.borderSvg[LayerComponentDiagramBorderElement.BORDER_RIGHT] = "l100,0";
        this.borderSvg[LayerComponentDiagramBorderElement.BORDER_UP] = "l0,-100";
        this.borderSvg[LayerComponentDiagramBorderElement.BORDER_DOWN] = "l0,100";
        this.borderSvg[LayerComponentDiagramBorderElement.INTERFACE_UP_RIGHT] = "c12.5,-50 87.5,-50 100,0";
        this.borderSvg[LayerComponentDiagramBorderElement.INTERFACE_UP_LEFT] = "c-12.5,-50 -87.5,-50 -100,0";

        this.borderSvg[LayerComponentDiagramBorderElement.INTERFACE_DOWN_RIGHT] = "c12.5,50 87.5,50 100,0";
        this.borderSvg[LayerComponentDiagramBorderElement.INTERFACE_DOWN_LEFT] = "c-12.5,50 -87.5,50 -100,0";

        this.borderSvg[LayerComponentDiagramBorderElement.INTERFACE_LEFT_DOWN] = "c-50,12.5 -50,87.5 0,100";
        this.borderSvg[LayerComponentDiagramBorderElement.INTERFACE_LEFT_UP] = "c-50,-12.5 -50,-87.5 0,-100";

        this.borderSvg[LayerComponentDiagramBorderElement.INTERFACE_RIGHT_DOWN] = "c50,12.5 50,87.5 0,100";
        this.borderSvg[LayerComponentDiagramBorderElement.INTERFACE_RIGHT_UP] = "c50,-12.5 50,-87.5 0,-100";
    }

    public createSvgTextElementString(node: LayerComponentDiagramNode): string {
        let sTextElements = "";
        const markers = node.getMarkers();
        markers.filter(marker => marker.properties).forEach(marker => {
            const x = node.getOffset().getX() + marker.getOffset().getX() + 1;
            const y = node.getOffset().getY() + marker.getOffset().getY();
            const properties = marker.properties;
            const textColor = properties.getProperty("text-color");
            const text = properties.getProperty("text");
            const className = properties.getProperty("class");

            let sTextElement = `<text x="${x * LayerComponentDiagramNodeSvgPathBuilder.FIELD_SIZE + LayerComponentDiagramNodeSvgPathBuilder.BORDER_PADDING}" y="${y * LayerComponentDiagramNodeSvgPathBuilder.FIELD_SIZE + LayerComponentDiagramNodeSvgPathBuilder.BORDER_PADDING}"`;
            if (textColor && textColor.length > 0) sTextElement += ` fill="${textColor}"`;
            if (className && className.length > 0) sTextElement += ` class="${className}"`;
            sTextElement += ">";
            if (text) sTextElement += text;
            sTextElement += "</text>";
            sTextElements = [sTextElements, sTextElement].join("\r\n");
        });
        return sTextElements;
    }

    public createSvgPathElementString(node: LayerComponentDiagramNode): string {
        const svgPath = this.createPath(node);
        let sElement = '<path';
        let fill = "white";
        let stroke = "black";
        let strokeWidth = "5";
        let className = "layout-component-node";
        const markers = node.getMarkers();
        if (markers && markers.length > 0) {
            const marker = markers[0];
            const properties = marker.properties;
            if (properties) {
                const propertyFill = properties.getProperty("fill");
                if (propertyFill && propertyFill.length > 0) fill = propertyFill;
                const propertyStroke = properties.getProperty("stroke");
                if (propertyStroke && propertyStroke.length > 0) stroke = propertyStroke;
                const propertyStrokeWidth = properties.getProperty("stroke-width");
                if (propertyStrokeWidth && propertyStrokeWidth.length > 0) strokeWidth = propertyStrokeWidth;
                const propertyClassName = properties.getProperty("class");
                if (propertyClassName && propertyClassName.length > 0) className = propertyClassName;
            }
        }
        if (fill && fill.length > 0) sElement += ` fill="${fill}"`;
        if (stroke && stroke.length > 0) sElement += ` stroke="${stroke}"`;
        if (strokeWidth && strokeWidth.length > 0) sElement += ` stroke-width="${strokeWidth}"`;
        if (className && className.length > 0) sElement += ` class="${className}"`;
        sElement += ` d="${svgPath}"`;
        sElement += ' />';
        return sElement;
    }

    public createPath(node: LayerComponentDiagramNode): string {
        const topLeftCorner = node.findTopLeftCorner();
        if (!topLeftCorner) throw new Error("No top left corner found for node");
        const startX = node.getOffset().getX() + topLeftCorner.getX() + 3;
        const startY = node.getOffset().getY() + topLeftCorner.getY();
        let svgPath = `M${startX * LayerComponentDiagramNodeSvgPathBuilder.FIELD_SIZE + LayerComponentDiagramNodeSvgPathBuilder.BORDER_PADDING},${startY * LayerComponentDiagramNodeSvgPathBuilder.FIELD_SIZE + LayerComponentDiagramNodeSvgPathBuilder.BORDER_PADDING}`;
        let first = true;
        const traversedBorderElementObserver = (borderElement: LayerComponentDiagramBorderElement): void => {
            if (first) {
                first = false;
                return;
            }
            const svgPart = this.borderSvg[borderElement];
            svgPath = [svgPath, svgPart].join(" ");
        };
        const borderTraverser = new LayerComponentDiagramAreaBorderTraverser(node, traversedBorderElementObserver);
        borderTraverser.traverseAndGetBorder(topLeftCorner);
        return svgPath;
    }
}