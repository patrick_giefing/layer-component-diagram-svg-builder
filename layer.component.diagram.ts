import {LayerComponentDiagramNode} from "./layer.component.diagram.node";

export class LayerComponentDiagram {
    public constructor(private nodes: LayerComponentDiagramNode[]) {
    }

    public getNodes(): LayerComponentDiagramNode[] {
        return this.nodes;
    }
}
