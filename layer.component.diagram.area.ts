import {DiagramNodePosition} from "./diagram.node.position";
import {DiagramElementDirection} from "./layer.component.diagram.reader";

export abstract class LayerComponentDiagramArea {
    private static borderSymbols = ["+", "^", "<", ">", "v", "°"];
    private readonly diagramString: string;
    private readonly width: number;
    private readonly height: number;
    private readonly lines: string[];

    constructor(diagramString: string) {
        this.diagramString = diagramString;
        this.lines = diagramString.split(/\r?\n/g);
        this.lines = this.lines ? this.lines : [];
        this.height = this.lines.length;
        this.width = Math.max.apply(null, this.lines.map(line => line.length));
    }

    public getSymbolAt(position: DiagramNodePosition): string {
        const isValidPosition = this.isValidPosition(position);
        if (!isValidPosition) return null;
        const x = position.getX();
        const y = position.getY();
        const line = this.lines[y];
        if (!line || x >= line.length) return null;
        const symbol = line[x];
        return symbol;
    }

    public getDiagramString(): string {
        return this.diagramString;
    }

    public getWidth(): number {
        return this.width;
    }

    public getHeight(): number {
        return this.height;
    }

    public isValidPosition(position: DiagramNodePosition): boolean {
        if (!position) return false;
        const x = position.getX();
        const y = position.getY();
        if (isNaN(x) || isNaN(y)) return false;
        if (x < 0 || y < 0) return false;
        const width = this.getWidth();
        const height = this.getHeight();
        if (x >= width || y >= height) return false;
        return true;
    }

    private areBorderSymbolsAt(points: DiagramNodePosition[]): boolean {
        for (const point of points) {
            const symbol = this.getSymbolAt(point);
            if (!LayerComponentDiagramArea.isBorderSymbol(symbol)) return false;
        }
        return true;
    }

    public isTopLeftCorner(position: DiagramNodePosition): boolean {
        return this.areBorderSymbolsAt([position, position.addOffset(DiagramNodePosition.of(1, 0)), position.addOffset(DiagramNodePosition.of(0, 1))]);
    }

    public isTopRightCorner(position: DiagramNodePosition): boolean {
        return this.areBorderSymbolsAt([position, position.addOffset(DiagramNodePosition.of(-1, 0)), position.addOffset(DiagramNodePosition.of(0, 1))]);
    }

    public isBottomLeftCorner(position: DiagramNodePosition): boolean {
        return this.areBorderSymbolsAt([position, position.addOffset(DiagramNodePosition.of(1, 0)), position.addOffset(DiagramNodePosition.of(0, -1))]);
    }

    public isBottomRightCorner(position: DiagramNodePosition): boolean {
        return this.areBorderSymbolsAt([position, position.addOffset(DiagramNodePosition.of(-1, 0)), position.addOffset(DiagramNodePosition.of(0, -1))]);
    }

    public isBorderRight(position: DiagramNodePosition): boolean {
        return this.areBorderSymbolsAt([position, position.addOffset(DiagramNodePosition.of(1, 0))]);
    }

    public isBorderDown(position: DiagramNodePosition): boolean {
        return this.areBorderSymbolsAt([position, position.addOffset(DiagramNodePosition.of(0, 1))]);
    }

    public isBorderLeft(position: DiagramNodePosition): boolean {
        return this.areBorderSymbolsAt([position, position.addOffset(DiagramNodePosition.of(-1, 0))]);
    }

    public isBorderUp(position: DiagramNodePosition): boolean {
        return this.areBorderSymbolsAt([position, position.addOffset(DiagramNodePosition.of(0, -1))]);
    }

    public static isBorderSymbol(symbol: string): boolean {
        const isBorderSymbol = LayerComponentDiagramArea.borderSymbols.indexOf(symbol) >= 0;
        return isBorderSymbol;
    }

    public static isContentStartMarkerSymbol(symbol: string): boolean {
        return symbol == "[";
    }

    public findTopLeftCorner(): DiagramNodePosition {
        const topLeftCorners = this.findTopLeftCorners();
        return topLeftCorners.length > 0 ? topLeftCorners[0] : null;
    }

    public findTopLeftCorners(): DiagramNodePosition[] {
        const topLeftCornerPositions: DiagramNodePosition[] = [];
        const diagramWidth = this.getWidth();
        const diagramHeight = this.getHeight();
        const lastPossibleTopLeftCornerStartPositionX = diagramWidth - 2;
        const lastPossibleTopLeftCornerStartPositionY = diagramHeight - 2;
        for (let x = 0; x < lastPossibleTopLeftCornerStartPositionX; x++) {
            for (let y = 0; y < lastPossibleTopLeftCornerStartPositionY; y++) {
                const position = DiagramNodePosition.of(x, y);
                const isTopLeftCorner = this.isTopLeftCorner(position);
                if (isTopLeftCorner)
                    topLeftCornerPositions.push(position);
            }
        }
        return topLeftCornerPositions;
    }
}

export class LayerComponentDiagramMarker {
    public properties: LayerComponentDiagramMarkerProperties;

    private constructor(private offset: DiagramNodePosition, private name: string) {
    }

    public static ofOffsetAndName(offset: DiagramNodePosition, name: string): LayerComponentDiagramMarker {
        return new LayerComponentDiagramMarker(offset, name);
    }

    public getOffset(): DiagramNodePosition {
        return this.offset;
    }

    public getName(): string {
        return this.name;
    }
}

// TODO Builder object
export class LayerComponentDiagramMarkerProperties {
    public constructor(private name: string, private properties: { [id: string]: string; }) {
    }

    public getName(): string {
        return this.name;
    }

    public getProperties(): { [id: string]: string; } {
        return this.properties;
    }

    public getProperty(propertyName: string): string {
        const properties = this.properties;
        if (!properties) return null;
        const propertyValue = properties[propertyName];
        return propertyValue;
    }
}

export class LayerComponentDiagramMarkerPropertiesReader {
    public static readString(s: string): { [id: string]: LayerComponentDiagramMarkerProperties; } {
        const markerProperties: { [id: string]: LayerComponentDiagramMarkerProperties; } = {};
        const reg = new RegExp(/\[([A-Za-z0-9_-]+):([^\]]+)\]/g);
        let result;
        while ((result = reg.exec(s)) !== null) {
            const name = result[1];
            const properties: { [id: string]: string; } = {};
            const propertiesString = result[2];
            const propertiesParts = propertiesString.split("|");
            for (const propertyPart of propertiesParts) {
                const seperatorIndex = propertyPart.indexOf("=");
                if (seperatorIndex > 0) {
                    const propertyName = propertyPart.substring(0, seperatorIndex);
                    const propertyValue = propertyPart.substring(seperatorIndex + 1);
                    properties[propertyName] = propertyValue;
                }
            }
            markerProperties[name] = new LayerComponentDiagramMarkerProperties(name, properties);
        }
        return markerProperties;
    }
}

export class LayerComponentDiagramAreaBorderTraverser {
    constructor(private drawingArea: LayerComponentDiagramArea, private traversedBorderElementObserver: (borderElement: LayerComponentDiagramBorderElement) => void = null) {
    }

    public traverseAndGetBorder(startPosition: DiagramNodePosition): boolean[][] {
        const width = this.drawingArea.getWidth();
        const height = this.drawingArea.getHeight();
        const border: boolean[][] = new Array(height).fill(false).map(() => new Array(width).fill(false));
        let borderSearchDirection = DiagramElementDirection.RIGHT;
        let position = startPosition.addOffset(DiagramNodePosition.of(1, 0));
        for(;;) {
            const isBorderAtPosition = border[position.getY()][position.getX()];
            if (isBorderAtPosition) break;
            border[position.getY()][position.getX()] = true;

            if (borderSearchDirection == DiagramElementDirection.RIGHT) {
                if (this.drawingArea.isBorderDown(position)) {
                    this.fireTraversedBorderElementObserver(LayerComponentDiagramBorderElement.TOP_TO_RIGHT, position);
                    position = position.addOffset(DiagramNodePosition.of(0, 1));
                    borderSearchDirection = DiagramElementDirection.DOWN;
                } else if (this.drawingArea.isBorderRight(position)) {
                    this.fireTraversedBorderElementObserver(LayerComponentDiagramBorderElement.BORDER_RIGHT, position);
                    position = position.addOffset(DiagramNodePosition.of(1, 0));
                } else if (this.drawingArea.isBorderUp((position))) {
                    this.fireTraversedBorderElementObserver(LayerComponentDiagramBorderElement.BOTTOM_TO_RIGHT, position);
                    position = position.addOffset(DiagramNodePosition.of(0, -1));
                    borderSearchDirection = DiagramElementDirection.UP;
                } else
                    return null;
            } else if (borderSearchDirection == DiagramElementDirection.DOWN) {
                if (this.drawingArea.isBorderLeft(position)) {
                    this.fireTraversedBorderElementObserver(LayerComponentDiagramBorderElement.RIGHT_TO_BOTTOM, position);
                    position = position.addOffset(DiagramNodePosition.of(-1, 0));
                    borderSearchDirection = DiagramElementDirection.LEFT;
                } else if (this.drawingArea.isBorderDown(position)) {
                    this.fireTraversedBorderElementObserver(LayerComponentDiagramBorderElement.BORDER_DOWN, position);
                    position = position.addOffset(DiagramNodePosition.of(0, 1));
                } else if (this.drawingArea.isBorderRight(position)) {
                    this.fireTraversedBorderElementObserver(LayerComponentDiagramBorderElement.LEFT_TO_BOTTOM, position);
                    position = position.addOffset(DiagramNodePosition.of(1, 0));
                    borderSearchDirection = DiagramElementDirection.RIGHT;
                } else
                    return null;
            } else if (borderSearchDirection == DiagramElementDirection.LEFT) {
                if (this.drawingArea.isBorderUp(position)) {
                    this.fireTraversedBorderElementObserver(LayerComponentDiagramBorderElement.BOTTOM_TO_LEFT, position);
                    position = position.addOffset(DiagramNodePosition.of(0, -1));
                    borderSearchDirection = DiagramElementDirection.UP;
                } else if (this.drawingArea.isBorderLeft(position)) {
                    this.fireTraversedBorderElementObserver(LayerComponentDiagramBorderElement.BORDER_LEFT, position);
                    position = position.addOffset(DiagramNodePosition.of(-1, 0));
                } else if (this.drawingArea.isBorderDown(position)) {
                    this.fireTraversedBorderElementObserver(LayerComponentDiagramBorderElement.TOP_TO_LEFT, position);
                    position = position.addOffset(DiagramNodePosition.of(0, 1));
                    borderSearchDirection = DiagramElementDirection.DOWN;
                } else
                    return null;
            } else /* UP */ {
                if (this.drawingArea.isBorderRight(position)) {
                    this.fireTraversedBorderElementObserver(LayerComponentDiagramBorderElement.LEFT_TO_TOP, position);
                    position = position.addOffset(DiagramNodePosition.of(1, 0));
                    borderSearchDirection = DiagramElementDirection.RIGHT;
                } else if (this.drawingArea.isBorderUp(position)) {
                    this.fireTraversedBorderElementObserver(LayerComponentDiagramBorderElement.BORDER_UP, position);
                    position = position.addOffset(DiagramNodePosition.of(0, -1));
                } else if (this.drawingArea.isBorderLeft(position)) {
                    this.fireTraversedBorderElementObserver(LayerComponentDiagramBorderElement.RIGHT_TO_TOP, position);
                    position = position.addOffset(DiagramNodePosition.of(-1, 0));
                    borderSearchDirection = DiagramElementDirection.LEFT;
                } else
                    return null;
            }
        }
        this.fireTraversedBorderElementObserver(LayerComponentDiagramBorderElement.BORDER_RIGHT, position);
        return border;
    }

    private fireTraversedBorderElementObserver(borderElement: LayerComponentDiagramBorderElement, position: DiagramNodePosition): void {
        if (!this.traversedBorderElementObserver) return;
        const symbol = this.drawingArea.getSymbolAt(position);
        if (symbol == "^") {
            if (borderElement == LayerComponentDiagramBorderElement.BORDER_LEFT) {
                borderElement = LayerComponentDiagramBorderElement.INTERFACE_UP_LEFT;
            } else if (borderElement == LayerComponentDiagramBorderElement.BORDER_RIGHT) {
                borderElement = LayerComponentDiagramBorderElement.INTERFACE_UP_RIGHT;
            }
        } else if (symbol == "v") {
            if (borderElement == LayerComponentDiagramBorderElement.BORDER_LEFT) {
                borderElement = LayerComponentDiagramBorderElement.INTERFACE_DOWN_LEFT;
            } else if (borderElement == LayerComponentDiagramBorderElement.BORDER_RIGHT) {
                borderElement = LayerComponentDiagramBorderElement.INTERFACE_DOWN_RIGHT;
            }
        } else if (symbol == "<") {
            if (borderElement == LayerComponentDiagramBorderElement.BORDER_UP) {
                borderElement = LayerComponentDiagramBorderElement.INTERFACE_LEFT_UP;
            } else if (borderElement == LayerComponentDiagramBorderElement.BORDER_DOWN) {
                borderElement = LayerComponentDiagramBorderElement.INTERFACE_LEFT_DOWN;
            }
        } else if (symbol == ">") {
            if (borderElement == LayerComponentDiagramBorderElement.BORDER_UP) {
                borderElement = LayerComponentDiagramBorderElement.INTERFACE_RIGHT_UP;
            } else if (borderElement == LayerComponentDiagramBorderElement.BORDER_DOWN) {
                borderElement = LayerComponentDiagramBorderElement.INTERFACE_RIGHT_DOWN;
            }
        } else if (symbol == "°") {
            if (borderElement == LayerComponentDiagramBorderElement.LEFT_TO_TOP) {
                borderElement = LayerComponentDiagramBorderElement.LEFT_TO_TOP_ROUND;
            } else if (borderElement == LayerComponentDiagramBorderElement.RIGHT_TO_BOTTOM) {
                borderElement = LayerComponentDiagramBorderElement.RIGHT_TO_BOTTOM_ROUND;
            } else if (borderElement == LayerComponentDiagramBorderElement.TOP_TO_LEFT) {
                borderElement = LayerComponentDiagramBorderElement.TOP_TO_LEFT_ROUND;
            } else if (borderElement == LayerComponentDiagramBorderElement.BOTTOM_TO_RIGHT) {
                borderElement = LayerComponentDiagramBorderElement.BOTTOM_TO_RIGHT_ROUND;
            } else if (borderElement == LayerComponentDiagramBorderElement.TOP_TO_RIGHT) {
                borderElement = LayerComponentDiagramBorderElement.TOP_TO_RIGHT_ROUND;
            } else if (borderElement == LayerComponentDiagramBorderElement.BOTTOM_TO_LEFT) {
                borderElement = LayerComponentDiagramBorderElement.BOTTOM_TO_LEFT_ROUND;
            } else if (borderElement == LayerComponentDiagramBorderElement.RIGHT_TO_TOP) {
                borderElement = LayerComponentDiagramBorderElement.RIGHT_TO_TOP_ROUND;
            } else if (borderElement == LayerComponentDiagramBorderElement.LEFT_TO_BOTTOM) {
                borderElement = LayerComponentDiagramBorderElement.LEFT_TO_BOTTOM_ROUND;
            }
        }
        this.traversedBorderElementObserver(borderElement);
    }
}

export enum LayerComponentDiagramBorderElement {
    TOP_TO_LEFT, TOP_TO_RIGHT, BOTTOM_TO_LEFT, BOTTOM_TO_RIGHT,
    TOP_TO_LEFT_ROUND, TOP_TO_RIGHT_ROUND, BOTTOM_TO_LEFT_ROUND, BOTTOM_TO_RIGHT_ROUND,
    LEFT_TO_TOP, RIGHT_TO_TOP, LEFT_TO_BOTTOM, RIGHT_TO_BOTTOM,
    LEFT_TO_TOP_ROUND, RIGHT_TO_TOP_ROUND, LEFT_TO_BOTTOM_ROUND, RIGHT_TO_BOTTOM_ROUND,
    BORDER_LEFT, BORDER_RIGHT, BORDER_UP, BORDER_DOWN,
    INTERFACE_UP_LEFT, INTERFACE_UP_RIGHT,
    INTERFACE_DOWN_LEFT, INTERFACE_DOWN_RIGHT,
    INTERFACE_LEFT_UP, INTERFACE_LEFT_DOWN,
    INTERFACE_RIGHT_UP, INTERFACE_RIGHT_DOWN
}